<?php namespace App\Providers;

use Auth;
use View;
use App\Category;
use App\Article;
use Illuminate\Validation\Validator;
use App\Validators\ScadEmailValidator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		// Add Scad Email Verification to system validation
		$this->app['validator']->resolver(function($translator, $data, $rules, $messages)
	    {
	      return new ScadEmailValidator($translator, $data, $rules, $messages);
	    });

	    /**
	     * Add Categories list to all pages for navigation
	     *
	     * TODO: Consider moving view coposers to it's own ComposerServiceProvider?
	     */
	    
	    // Give categories to the navigation view
	    View::composer('partials.navigation', function($view )
        {
        	$categories = Category::all();

            $user = Auth::user();

            // Approve Story Count if necessary
            $approve_count = '';
            if ( $user->can('moderate-story') ) {
                $approve_count = Article::unpublished()->count();
            }

        	$view->with( compact( 'categories', 'user', 'approve_count' ) );
        });

        // Give user to header view
        View::composer('partials.header', function( $view )
        {
        	$user = Auth::user();

        	$view->with( compact( 'user' ) );
        });
	    
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
