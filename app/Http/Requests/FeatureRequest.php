<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class FeatureRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // open to anyone
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image'         => 'image|max:4000',
            'title'         => 'required|min:3',
            'description'   => 'required|min:3'
        ];
    }

    /**
     * Custom messages for validation rules
     */
    public function messages()
    {
        return [
            'image.image'       => 'That cover photo needs to be an image.',
            'image.max'         => 'That cover is way too big.  Can we keep it under 4 MB?',
            'image.required'    => 'Oh dang, we need a cover photo.',
        ];
    }

    /**
     * Extend the validator instance to 
     * attach a custom Sometimes() condition
     */
    protected function getValidatorInstance()
    {
        // Get the original validator
        $validator = parent::getValidatorInstance();

        // Add a condition
        $validator->sometimes('image', 'required', function( $input )
        {
            // Validate only if there's
            // no existing photo saved
            return ( ! $input->hasPhoto );
        });

        // Return it
        return $validator;
    }

}
