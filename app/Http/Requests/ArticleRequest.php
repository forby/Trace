<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArticleRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		// open to anyone
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'photo'			=> 'image|max:4000',
			'title'        	=> 'required|min:3',
			'overview'     	=> 'required|min:3',
			'example'     	=> 'required|min:3',
			'advice'		=> 'required|min:3'
		];
	}

	/**
	 * Custom messages for validation rules
	 */
	public function messages()
	{
		return [
			'photo.image'		=> 'That cover photo needs to be an image.',
			'photo.max'			=> 'That photo is huuuuge.  Can we keep it under 4 MB?',
			'photo.required' 	=> 'Oh dang, we need a cover photo.',
		];
	}

	/**
	 * Extend the validator instance to 
	 * attach a custom Sometimes() condition
	 */
	protected function getValidatorInstance()
	{
		// Get the original validator
	    $validator = parent::getValidatorInstance();

	    // Add a condition
	    $validator->sometimes('photo', 'required', function( $input )
	    {
	    	// Validate only if there's
	    	// no existing photo saved
	        return ( ! $input->hasPhoto );
	    });

	    // Return it
	    return $validator;
	}

}
