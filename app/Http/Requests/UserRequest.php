<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // open to anyone
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo'         => 'image|max:2000',
        ];
    }

    /**
     * Custom messages for validation rules
     */
    public function messages()
    {
        return [
            'photo.image'       => 'That photo needs to be an image.',
            'photo.max'         => 'That photo is huuuuge.  Can we keep it under 2 MB?',
        ];
    }

}
