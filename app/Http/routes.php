<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Testing the medium editor
Route::get('editor', ['middleware' => 'auth', function() {
	return view('test.editor');
}]);

// Zoho Verify
Route::get('zohoverify/zohoverify.html', function() {
    return '1443551183895';
});

// Images
Route::post('image/upload', 'ImageController@upload');
Route::post('image/delete', 'ImageController@delete');

// API
Route::post('api/comment/new', 'CommentController@store');
Route::get('api/comment/{id}/delete', 'CommentController@destroy');
Route::get('api/article/bump/{article}', 'ArticlesController@bump');
Route::get('api/article/unbump/{article}', 'ArticlesController@unbump');
Route::post('api/article/feature/toggle', 'ArticlesController@toggleFeature');
Route::post('api/user/{id}/update-notify', 'UserController@updateNotify');
Route::post('api/user/{id}/remove', 'UserController@destroy');

// Basic Pages
Route::get('/', 'HomeController@index');
Route::get('category/{id}', 'CategoriesController@index');
Route::get('tag/{tag}', 'TagsController@show');
Route::get('about', function() { return view('about'); });
Route::get('help', function() { return view('help'); });
Route::get('legal', function() { return view('legal'); });

// Send us a message
Route::post('contact/send', 'ContactController@send');

// Articles Resource, with a singular option
Route::resource('articles', 'ArticlesController');
Route::get('article/pending', 'ArticlesController@pending');
Route::get('article/{articles}', 'ArticlesController@show');
Route::get('article/{articles}/edit', 'ArticlesController@edit');
Route::get('article/{articles}/publish', 'ArticlesController@publish');
Route::post('article/{articles}/request', 'ArticlesController@sendRequest');
Route::post('article/{articles}/delete', 'ArticlesController@destroy');
Route::get('top-stories', 'ArticlesController@topStories');

// Management
Route::get('approve', 'ManagementController@index');
Route::get('manage-users', 'UserController@manage');
Route::post('manage-users/make-editor', 'UserController@makeEditor');
Route::post('manage-users/make-user', 'UserController@makeUser');
Route::post('manage-users/create-redirect', 'UserController@createRedirect');
Route::post('manage-users/update-redirect', 'UserController@updateRedirect');
Route::post('manage-users/remove-redirect', 'UserController@removeRedirect');

// Feature
Route::get('feature', 'FeatureController@index');
Route::get('feature/edit', 'FeatureController@edit');
Route::patch('feature/edit', 'FeatureController@update');
Route::delete('feature/edit', 'FeatureController@destroy');

// User Profile
Route::get('user/{user}', 'UserController@index');
Route::get('user/{user}/written', 'UserController@written');
Route::get('user/{user}/bumped', 'UserController@bumped');
Route::get('user/{user}/commented', 'UserController@commented');
Route::get('user/{user}/edit', 'UserController@edit');
Route::post('user/{user}/edit', 'UserController@update');
Route::get('user/{user}/delete', 'UserController@delete');

// User Settings
Route::get('settings', 'UserController@settings');

// Register Confirmation
Route::get('register/confirm', 'Auth\AuthController@wait');
Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'Auth\AuthController@confirm'
]);

// Login / Register
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');
Route::post('register', 'Auth\AuthController@postRegister');
Route::get('login/cas', 'Auth\AuthController@getCasLogin');
Route::get('logout/cas', 'Auth\AuthController@getCasLogout');

// Password Reset
Route::get('forgot', 'Auth\PasswordController@getEmail');
Route::post('forgot', 'Auth\PasswordController@postEmail');
Route::get('reset', 'Auth\PasswordController@getEmail');
Route::post('reset', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// Redirects
// Make sure this is the last route entry
Route::get('/{slug}', ['uses' => 'RedirectController@checkRedirect']);
