<?php namespace App\Http\Controllers;

use Auth;
use File;
use App\Article;
use App\Feature;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\FeatureRequest;

class FeatureController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$feature = Feature::active()->first();

		// If there is no feature, go home
		if ( ! $feature ) {
			return redirect('/');
		}

		// Get the articles
		$articles = Article::featured()->latest()->paginate(10);

		// Get Feature Title
		$title = $feature->title;

		return view('feature.index', compact('articles', 'title'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		// Who are you?
		$user = Auth::user();

		// Should you be here?
		if ( ! $user->can('set-featured-content') ) {
		    return redirect('/');
		}

		// Get the feature object
		$feature = Feature::active()->first();

		return view('feature.edit', compact('feature'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update( FeatureRequest $request )
	{
		// Do we already have a feature?
		$feature = Feature::active()->first();

		// Image Handling
		if ( $request->hasFile('image') )
		{
			// Get the image
			$photo = $request->file('image');

			// Get the image name
			$name = $photo->getClientOriginalName();

			// Delete old photo
			if ( $feature )
			{
				$this->deletePhoto( $feature->photo );
			}

			// Save photo to server
			$photo->move('img/features', $name);

			// Add photo to request 
			// and some other default values
			$request->merge( [ 
				'photo' 		=> url('img/features/' . $name),
				'button_text'	=> 'READ MORE',
				'url'			=> 'feature',
				'active'		=> true
			] );
		}

		if ( ! $feature ) {
		    // If not, create it
		    Feature::create( $request->all() );

		    // And deactivate the default
		    // $feature = Feature::find(1);
		    // $feature->active = false;
		    // $feature->save();
		} else {
		    // If so, update it
		    $feature->update( $request->all() );
		}

		// If reset is checked, un-feature all current articles
		if ( $request->get('reset') ) {
			Article::featured()->get()->each(function( $article ) {
				$article->unfeature();
			});
		}

		// Go back home
		return redirect('/');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		// find custom feature
		$feature = Feature::active()->first();

		if ( $feature ) {

		    // Delete the image
		    $this->deletePhoto( $feature->photo );

		    // Remove the feature
		    $feature->delete();
		}

		// Reset all featured articles
		Article::featured()->get()->each(function( $article ) {
			$article->unfeature();
		});

		return redirect('/');
	}

	private static function removeUrlFromPath( $path )
	{
		return substr( $path, strpos( $path, 'img' ) );
	}

	public function deletePhoto( $path ) {
		$filePath = public_path() . '/' . $this->removeUrlFromPath( $path );

		if ( File::exists( $filePath ) ) {
			File::delete( $filePath );
		}
	}

}
