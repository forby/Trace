<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
// use Request;
use Response;
use App\Role;
use App\User;
use App\Comment;
use App\Article;
use App\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;

class UserController extends Controller {

	public function __construct()
	{
	    $this->middleware('auth');
	}

	/**
	 * Display a User Profile.
	 *
	 * @return Response
	 */
	public function index( $slug )
	{
		// Consider route/model binding
		// instead of getting the user here
		$user = null;
		
		if (is_numeric($slug) && $slug > 0) {
			$user = User::withTrashed()->where('id', $slug)->firstOrFail();
		} else {
			$user = User::withTrashed()->whereSlug( $slug )->firstOrFail();
		}

		// If trashed go home
		if ( $user->trashed() )
		{
			return redirect('/');
		}
		

		$get_count = 3;

		// Get Articles that this user has commented on		
		$articles_commented = $user->commented->take( $get_count )->get();
		$articles_commented_count = $user->commentedCount;

		// Get Articles that this user has bumped
		$articles_bumped = $user->bumped->take( $get_count )->get();
		$articles_bumped_count = $user->bumpedCount;

		// Get Articles that this user has written
		if ( Auth::user()->slug == $slug )
		{
			$articles_written = $user->articles()->latest()->take( $get_count )->get();
			$articles_written_count = $user->articles()->count();
		} else {
			$articles_written = $user->articles()->published()->latest()->take( $get_count )->get();
			$articles_written_count = $user->articles()->published()->count();
		}
		

		return view( 'user.show', compact(
			'user', 
			'articles_commented',
			'articles_commented_count',
			'articles_bumped',
			'articles_bumped_count',
			'articles_written',
			'articles_written_count'
		));
	}

	/**
	 * Show Archive of Articles that this user has written
	 */
	public function written( $slug )
	{
		$user = User::findBySlugOrIdOrFail( $slug );

		if ( Auth::user()->slug == $slug )
		{
			$articles = $user->articles()->latest()->paginate(10);
		} else {
			$articles = $user->articles()->published()->latest()->paginate(10);
		}

		return view( 'user.written', compact(
			'user',
			'articles'
		));
	}

	/**
	 * Show Archive of Articles that this user has bumped
	 */
	public function bumped( $slug )
	{
		$user = User::findBySlugOrIdOrFail( $slug );

		$articles = $user->bumped->paginate(10);

		return view( 'user.bumped', compact(
			'user',
			'articles'
		));
	}

	/**
	 * Show Archive of Articles that this user has commented on
	 */
	public function commented( $slug )
	{
		$user = User::findBySlugOrIdOrFail( $slug );

		$articles = $user->commented->paginate(10);

		return view( 'user.commented', compact(
			'user',
			'articles'
		));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit( $slug )
	{
		$user = User::findBySlugOrIdOrFail( $slug );

		return view( 'user.edit', compact('user'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update( UserRequest $request )
	{
		// Get the user
		$user = User::findBySlugOrIdOrFail( $request->get('user_id') );

		if ( $request->hasFile('photo') ) 
		{
			// Get the Photo
			$photo = $request->file('photo');

			// Get full filename, like 'name.jpg'
			$name = $photo->getClientOriginalName();

			// Add an ending id, like 'name_id14.jpg'
			$ending_id = '_id' . $user->id;
			$new_name = substr_replace($name, $ending_id, strrpos($name, '.'), 0);

			// Move Save the photo to 'img/articles/name_id14.jpg'
			$photo->move('img/users', $new_name);

			// Remove the old photo file
			if ( $user->avatar )
			{
				$this->deletePhoto( $user->avatar );
			}

			// Store the reference to the image
			// with a full URL path
			$request->merge( ['avatar' => url('img/users/' . $new_name)] );
		} 
		else if ( $request->has('remove_avatar') && $user->avatar ) 
		{
			// If there's no file input
			// but still requests to remove photo
			// just simply remove it and move on
			$this->deletePhoto( $user->avatar );

			// Add in a blank avatar entry for the avatar
			$request->merge( ['avatar' => null] );
		}

		// If the user has input a custom name
		// add it to the request to be updated
		if ( $request->has('name') && $request->input('name') !== $user->first_name.' '.$user->last_name ) {
			$request->merge(['custom_name' => $request->input('name')]);
		} else if ( !$request->has('name') ||  $request->input('name') == $user->first_name.' '.$user->last_name ) {
			// remove the custom name
			$request->merge(['custom_name' => null]);
		}

		// Update the info
		$user->update( $request->except(['user_id', 'name']) );
		
		return redirect()->action('UserController@index', $user->slug);
	}

	/**
	 * Confirmation view for deleting your account
	 */
	public function delete( $id )
	{
		// Get the user
		$user = User::findBySlugOrIdOrFail( $id );

		return view('user.bye', compact('user'));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy( $id )
	{
		$loggedUser = Auth::user();

		// Can you do this?
		if ( $loggedUser->can('manage-users') || $loggedUser->id == $id )
		{
			// Who do we delete?
			$user = User::findBySlugOrIdOrFail( $id );

			// Bye bye :(
			$user->delete();

			return Response::json('success', 200);
		}

		return Response::json('error', 400);
	}

	/**
	 * Update Email notification settings
	 */
	public function updateNotify( $id )
	{
		// Get the user
		$user = User::findBySlugOrIdOrFail( $id )->first();

		// Update the notification setting
		$user->email_notify = Request::has('email_notify');
		$user->save();

		return Response::json('success', 200);
	}

	/**
	 * Show the settings for this User
	 */
	public function settings()
	{
		$user = Auth::user();

		return view( 'user.settings', compact('user') );
	}

	/**
	 * Remove the URL from an image location
	 */
	private static function removeUrlFromPath( $path )
	{
		return substr( $path, strpos( $path, 'img' ) );
	}

	/**
	 * Delete a photo from the filesystem
	 */
	public function deletePhoto( $path ) {
		$filePath = public_path() . '/' . $this->removeUrlFromPath( $path );

		if ( File::exists( $filePath ) ) {
			File::delete( $filePath );
		}
	}

	/**
	 * Return a view for Admins to manage user permissions
	 */
	public function manage()
	{
		// Who are you?
		$user = Auth::user();

		// Should you be here?
		if ( !$user->can('manage-users') ) {
			return redirect('/');
		} 

		// Get the Administrators
		$admins = Role::with('users')->where('name','admin')->first()->users;

		// Get the Editors
		$editors = Role::with('users')->where('name','editor')->first()->users;

		// remove admins...
		// how to do this better?
		$editors = $editors->reject(function ($editor) {
			// reject if...
			return $editor->hasRole('admin');
		});

		// Get the normal Users
		$users = Role::with('users')->where('name', 'user')->first()->users;

		// Remove editors
		$users = $users->reject(function ($user) {
			// reject if...
			return $user->hasRole('editor');
		});

		$users = $users->lists('name', 'id');

		// Get umber of Articles
		$totalBumps = Article::getTotalBumps();

		// Get number of Comments
		$totalComments = 0;
		$articles = Article::published()->with('comments')->get();
		foreach ( $articles as $article ) {
			$totalComments += $article->comments->count();
		}

		// Get the Redirects
		$redirects = Redirect::orderBy('created_at', 'DESC')->get();

		return view('user.manage', compact('admins', 'editors', 'users', 'totalBumps', 'totalComments', 'redirects'));
	}

	/**
	 * Create a new Redirect
	 *
	 * TODO - Consider moving this to a new controller?
	 */
	public function createRedirect( Request $request )
	{

		// Validate
		$this->validate($request, [
	        'name' => 'required|unique:redirects',
	        'url' => 'required',
	    ]);

		// Create a new request
		$flight = Redirect::create([
			'name'	=> str_slug( $request->name, '-' ),
			'url'	=> $request->url
		]);

		// reload the manage users page
		return redirect('manage-users');
	}

	/**
	 * Update a new Redirect
	 *
	 * TODO - Consider moving this to a new controller?
	 */
	public function updateRedirect( Request $request )
	{

		// Validate
		$this->validate($request, [
	        'url' => 'required|filled',
	    ]);

		// Get the redirect
		$redirect = Redirect::find( $request->id );

		// Update
		$redirect->url = $request->url;
		$redirect->save();

		// reload the manage users page
		return redirect('manage-users');
	}

	/**
	 * Remove a new Redirect
	 *
	 * TODO - Consider moving this to a new controller?
	 */
	public function removeRedirect( Request $request )
	{
		// Delete the item
		Redirect::destroy( $request->id );

		// reload the manage users page
		return redirect('manage-users');
	}

	/**
	 * Update User Role to Editor
	 */
	public function makeEditor( Request $request)
	{
		// Get the user
		$user = User::find( $request->get('id') );

		// Attach the editor role
		$user->attachRole(2);

		// reload the manage users page
		return redirect('manage-users');
	}

	/**
	 * Update Editor Role to User
	 */
	public function makeUser( Request $request)
	{
		// Get the user
		$user = User::find( $request->get('id') );

		// Remove the editor role
		$user->detachRole(2);

		// reload the manage users page
		return redirect('manage-users');
	}

}
