<?php namespace App\Http\Controllers;

use Auth;
use File;
use App\Article;
use App\Feature;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\FeatureRequest;

class ManagementController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
    {
        // Who are you?
        $user = Auth::user();

        // Should you be here?
        if ( ! $user->can('moderate-story') ) {
            return redirect('/');
        }

        // Get the articles
        $articles = Article::unpublished()->latest()->paginate(12);

        // Show list of articles to approve
        return view('articles.approve', compact('articles'));
    }   

}
