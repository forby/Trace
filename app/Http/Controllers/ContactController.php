<?php namespace App\Http\Controllers;

use Auth;
use Mail;
use App\Role;
use Response;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller {

	public function __construct()
    {
        // $this->middleware('auth');
    }

    protected function getRedirectUrl()
    {
        return app('Illuminate\Routing\UrlGenerator')->previous() . '#contact-form';
    }

    public function send( Request $request )
    {

        $this->validate(
            $request, 
            [
                'email'   => 'required|email|max:255',
                'name'    => 'required|min:2',
                'subject' => 'required|min:2',
                'message' => 'required|min:2',
            ], 
            [
                'email.required'   => 'We\'re really going to need that email address.',
                'name.required'    => 'How about that name?',
                'email.email'      => 'Whoops, we need a REAL email address.',
                'subject.required' => 'Oops, we need a subject.',
                'message.required' => 'And... didn\'t you want to say anything?'
            ]
        );

        // Get Admin Emails
        $admins = Role::with('users')->where('name','admin')->first()->users()->lists('email')->all();

        // Send the Email
        Mail::send('emails.contact', [
                'name'    => $request->get('name'),
                'email'   => $request->get('email'),
                'subject' => $request->get('subject'),
                'body'    => $request->get('message')
            ], function( $message ) use ( $request, $admins ) {
            $message->to( $admins );
            $message->subject('New contact message from Trace');
        });
        
        // Return with a message
        return redirect()->to($this->getRedirectUrl())->with(['sent' => 'Got it! We\'ll be in touch!']);
    }

}
