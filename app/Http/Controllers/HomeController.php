<?php namespace App\Http\Controllers;

use App\Tag;
use App\Feature;
use App\Article;
use App\Category;
use Carbon\Carbon;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		// Get 5 latest articles to show on the homepage
		$articles = Article::published()->latest()->with('user')->paginate(5);

		// Set the "view archive" pagination link
		$articles->setPath('articles');

		// Get top 10 tags sorted by article count
		$top_tags = Tag::getTopTags()->take(10);

		// Get a Collection of 20 super bumped articles
		$super_bumps = Article::superBumped()->take(20)->get();

		// Choose a random one to feature
		if ( $super_bumps->count() > 0 ) {
			$feature_article = $super_bumps->random();
		}

		// Get the first 5 super bumped articles to list as top
		$top_articles = $super_bumps->slice(0, 5);

		// Get the current Feature object
		$feature = Feature::active()->first();

		if ( ! $feature ) {
			$feature = Feature::first();
		}
		
		return view('home', compact('articles', 'feature_article', 'top_tags', 'top_articles', 'feature'));
	}

}
