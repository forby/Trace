<?php namespace App\Http\Controllers;

use App\Tag;
use App\Article;
use App\Category;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index( $slug )
    {
        // eager load in category instead of finding it here from ID...
        $category = Category::where('slug', $slug)->firstOrFail();

        // Get the articles
        $articles = $category->articles()->published()->paginate(10);

        // Get top 10 tags sorted by article count
        $top_tags = Tag::getTopTags()->take(10);

        $pagination = true;

        return view('category', compact('category', 'articles', 'top_tags'));
    }

}
