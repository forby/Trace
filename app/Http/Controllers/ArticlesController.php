<?php namespace App\Http\Controllers;

use App;
use Auth;
use File;
use Mail;
use App\Tag;
use App\User;
//use Request;
use Response;
use App\Article;
use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use Illuminate\Http\Request;

class ArticlesController extends Controller {

	public function __construct()
	{
		// limit public view
		$this->middleware('auth');
	}

	/**
	 * Display the archive of articles, 10 to a page
	 *
	 * @return Response
	 */
	public function index()
	{
		// Get the articles
		$articles = Article::published()->latest()->paginate(10);

		// Get top tags
		$top_tags = Tag::getTopTags()->take(10);
		
		return view('articles.index', compact('articles', 'top_tags'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// Get the user
		$user = Auth::user();

		// Make a list of users for the select box
		$users = User::get()->lists('name', 'id');

		// Get the list of tags
		$tags = Tag::lists('name', 'id')->all();

		// Get the list of categories
		$categories = Category::lists('name', 'id')->all();

		// Show the view
		return view('articles.create', compact('tags', 'categories', 'user', 'users'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store( ArticleRequest $request )
	{
		$user = '';

		// Get the user
		// If not live, allow admins to post as other users
		if ( App::environment('local', 'staging') && $request->has('post_as') ) {
			$user = User::find( $request->get('post_as') );
		} else {
			$user = Auth::user();
		}

		// Create and Save a new article
		$this->createArticle( $request, $user );

		// Reindex Tags
		Tag::reindex();

		// TODO: send email to editors?

		// show the pending response
		return redirect()->action('ArticlesController@pending');
	}

	/**
	 * Display the specified resource.
	 * Article is injected through Route Model Binding
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show( $article )
	{

		// Get a list of this article's tags
		$tag_ids = $article->tags()->lists('id')->all();

		// Find articles in those tags, except for this tag
		// Sort them randomly
		$relatedArticles = Article::published()->where('id', '!=', $article->id)
			->whereHas('tags', function($q) use ($tag_ids) {
		    	$q->whereIn('id', $tag_ids);
			})
			->orderByRaw('RAND()')
			->take(3)
			->get();

		$user = Auth::user();

		return view('articles.show', compact('article', 'relatedArticles', 'user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit( Article $article )
	{
		// Get the tags of the article
		$tags = Tag::lists('name', 'id')->all();

		// Get the categories
		$categories = Category::lists('name', 'id')->all();

		// Get the currently logged in user
		$user = Auth::user();

		// Show the edit view
		return view ('articles.edit', compact('article', 'tags', 'categories', 'user'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update( Article $article, ArticleRequest $request )
	{

		// Save the image
		$this->savePhoto( $request, $article);

		// Unpublish the article
		if ( $request->has('published') )
		{
			$request->repalce(['published' => false]);
		} else {
			$request->merge(['published' => false]);
		}

		// Update the article in DB
		$article->update( $request->all() );

		// Sync up the tags
		$this->syncTags( $article, $request->input('tag_list') );

		// Reindex Tags
		Tag::reindex();

		// show the pending response
		return redirect()->action('ArticlesController@pending');

		// TODO:
		// 	- Show a message that it has been sent for review
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy( Article $article, Request $request )
	{
		if ( Auth::user()->can('moderate-story') )
		{
			// Bye :(
			$article->delete();

			// Reindex Tags
			Tag::reindex();
		}

		if ( $request->ajax() )
		{
			return Response::json([
				'url' => url('/approve')
			], 200);
		} else {
			return redirec('/approve');
		}
	}

	/**
	 * Approve and Publish an article
	 */
	public function publish( $article )
	{
		// Get the user
		$user = Auth::user();

		// Only approve if you have the permissions
		if ( $user->can('moderate-story') )
		{
			$article->published = true;
			$article->save();

			// Send email to author
			// if user is not the author
			$author = $article->user;

			if ( $user->id !== $author->id ) {
				if ( $author->email ) {
					Mail::send('emails.published', compact('author', 'article'), function( $message ) use ( $author ) {
			            $message->to($author->email, $author->name);
			            $message->subject('Your article has been published');
			        });
				}
			}

			// Reindex Tags
			Tag::reindex();
		}

		return redirect('/approve');
	}

	/**
	 * Send the request from an ajax request
	 * and return json response
	 */
	public function sendRequest( Article $article, Request $request )
	{
		if ( Auth::user()->can('moderate-story') ) {
			// Get the body of the message
			$body = $request->get('message');

			// And the author
			$author = $article->user;

			// send mail to author
			if ( $author->email ) {
				Mail::send('emails.request-edits', compact('body','author', 'article'), function( $message ) use ( $author ) {
		            $message->to($author->email, $author->name);
		            $message->subject('A few things about your article...');
		        });
		    }

	        // Flash the message
	        // flash()->success('Your message has been sent!');
	        
	        // Success
	        return Response::json( 'success', 200 );
		}

		// Return Error
		return Response::json( 'error', 400 );
	}

	/**
	 * Show a 'Thanks' message to the user
	 * after they have created or edited an article
	 */
	public function pending()
	{
		return view('articles.pending');
	}

	/**
	 * API request for liking an article.
	 *
	 * @param int $article
	 */
	public function bump( $article )
	{
		// Like the Article
		// Uses currently logged in user.
		$article->like();

		// return proof
		if ( $article->liked() )
		{
			// Get author
			$author = $article->user;

			// Get the current user
			$user = Auth::user();

			// Send email to the author
			// if they are not the bumper
			// and only if they want emails
			if ( ($user->id !== $author->id) && ($author->email_notify) ) {
				if ( $author->email ) {
					Mail::send('emails.bump', compact('article'), function( $message ) use ( $author ) {
			            $message->to($author->email, $author->name);
			            $message->subject('You\'ve been bumped!');
			        });
				}
			}
			        
			return Response::json( [
				'count' => $article->likeCount
			], 200 );
		}

		// Error if not liked
		return Response::json('error', 400);
	}

	/**
	 * API request for un-liking an article.
	 *
	 * @param int $article
	 */
	public function unbump( $article )
	{
		// Like the Article
		// Uses currently logged in user.
		$article->unlike();

		// return proof
		if ( ! $article->liked() )
		{
			return Response::json( [
				'count' => $article->likeCount
			], 200 );
		}

		// Error if not liked
		return Response::json('error', 400);
	}

	/**
	 * API request for featuring an article
	 */
	public function toggleFeature( Request $request )
	{
		// Should you be here?
		if ( ! Auth::user()->can('set-featured-content') ) {
			return Response::json('error', 400);
		}

		// Find article and feature it
		// Article::find( Request::get('id') )->feature();
		
		$article = Article::find( $request->get('id') );
		$result = $article->toggleFeatured();
		$classes = $article->featureClasses;

		return Response::json([
			'classes' => $classes
		], 200 );
	}

	/**
	 * Create and save an article object from a request
	 * 
	 * @param  ArticleRequest $request
	 * @return Article
	 */
	private function createArticle( ArticleRequest $request, $user )
	{
		// Create the article from the request input
		$article = $user->articles()->create( $request->all() );

		// Sync up tags too
		$this->syncTags( $article, $request->input('tag_list') );

		// Save the Image to the server
		$this->savePhoto( $request, $article );

		return $article;
	}

	/**
	 * Perform sync an article's tags
	 * 
	 * @param  Article $article
	 * @param  array   $tags
	 * @return null
	 */
	private function syncTags( Article $article, array $tags=null )
	{
		$tag_ids = array();

		// create any new tags if they don't exist
		if ( $tags ) {
			foreach ( $tags as $tag ) {
				if ( ! Tag::find( $tag ) ) {
					$new =  Tag::create(['name' => $tag]);
					array_push( $tag_ids, strval($new->id) );
				} else {
					array_push( $tag_ids, $tag );
				}
			}	
		}

		// Sync the tags
		$article->tags()->sync( $tag_ids );
	}

	/**
	 * Save article feature photo
	 */
	private function savePhoto( $request, $article )
	{
		// Save the new file and reference
		if ( $request->hasFile('photo') )
		{
			// Get the Photo
			$photo = $request->file('photo');

			// Get full filename, like 'name.jpg'
			$name = $photo->getClientOriginalName();

			// Add an ending id, like 'name_id14.jpg'
			$ending_id = '_id' . $article->id;
			$new_name = substr_replace($name, $ending_id, strrpos($name, '.'), 0);

			// Move Save the photo to 'img/articles/name_id14.jpg'
			$photo->move('img/articles', $new_name);

			// Remove the old photo file
			if ( $article->photo )
			{
				$this->deletePhoto( $article->photo );
			}

			// Store the reference to the image
			// with a full URL path
			$article->photo = url('img/articles/' . $new_name);
			$article->save();
		}
	}

	/**
	 * Remove the URL from an image location
	 */
	private static function removeUrlFromPath( $path )
	{
		return substr( $path, strpos( $path, 'img' ) );
	}

	/**
	 * Delete a photo from the filesystem
	 */
	public function deletePhoto( $path ) {
		$filePath = public_path() . '/' . $this->removeUrlFromPath( $path );

		if ( File::exists( $filePath ) ) {
			File::delete( $filePath );
		}
	}

	/**
	 * Top Stories Page
	 */
	public function topStories()
	{
		$articles = Article::getTopArticles()->take(20);

		return view('articles.top', compact('articles'));
	}
}
