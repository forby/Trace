<?php namespace App\Http\Controllers;

use App\Tag;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class TagsController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function show( $slug )
    {
        // eager-load/inject in category instead of finding it here from ID...
        $tag = Tag::where('slug', $slug)->firstOrFail();

        // Get the tagged articles
        $articles = $tag->articles()->published()->paginate(10);

        // Get top 10 tags sorted by article count
        $top_tags = Tag::getTopTags()->take(10);

        return view('tag', compact('tag', 'articles', 'top_tags'));
    }

}
