<?php namespace App\Http\Controllers\Auth;

use App;
use Cas;
use URL;
use Auth;
use Mail;
use Config;
use Session;
use App\User;
use Response;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	protected $redirectTo = '/';
	protected $loginPath = '/login';

    protected function getFailedLoginMessage()
    {
        return 'That doesn\'t look right. We can\'t find anyone with this info.';
    }

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct()
	{
        $this->redirectAfterLogout = '/logout/cas';

		$this->middleware('guest', ['except' => 'getLogout']);
	}

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make(
            $data, 
            [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users', //|scad_email
                'password' => 'required|confirmed|min:6',
            ], 
            [
                'name.required' => 'No dice. We need a name.',
                'email.required' => "We're really going to need that email address.",
                'email.email' => 'Whoops, we need a REAL email address.',
                'password.required' => 'Did you forget your password?',
                'password.confirmed' => 'Those passwords have to match, bud.'
            ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function create(array $data)
    {

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => $data['confirmation_code'],
        ]);
    }

	/**
	 * Exted the Register function from trait
	 * 
	 * @param  Request $request 
	 * @return mixed
	 */
	public function postRegister(Request $request)
	{
		$validator = $this->registrar->validator($request->all());

		if ($validator->fails())
		{
            // add an indicator that we 
            // came from the registration form
            $request->merge(['register' => 'yes']);

            // continue with error
			$this->throwValidationException(
				$request, $validator
			);
		}

		// Create confirmation code
		$confirmation_code = array( 'confirmation_code' => str_random(30) );

		// Add code to create method
		$this->registrar->create( array_merge( $request->all(), $confirmation_code ) );

		// send mail
		Mail::send('emails.verify', $confirmation_code, function( $message ) use ( $request ) {
            $message->to($request->input('email'), $request->input('name'));
            $message->subject('Verify your email address');
        });

		// flash('Thanks for signing up! Please check your email.');

		return redirect('register/confirm')->with('name', $request->input('name'));
	}

	/**
	 * Overwrite == Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'email' => 'required|email', 'password' => 'required', // |scad_email
		], [
            'email.required'    => "We're really going to need that email address.",
            'email.email'       => 'Whoops, we need a REAL email address.',
            'password.required' => 'Did you forget your password?'
        ]);

		// Add in Confirmed credentials
		$credentials = array_merge( $request->only('email', 'password'), array( 'confirmed' => 1 ));

		if ($this->auth->attempt($credentials, $request->has('remember')))
		{
			// flash('You have successfully signed in!');
			return redirect()->intended($this->redirectPath());
		}

		return redirect($this->loginPath())
					->withInput($request->only('email', 'remember'))
					->withErrors([
						'email' => $this->getFailedLoginMessage(),
						'credentials' => 'You\'re getting ahead of yourself. You have to confirm your email before logging in.'
					]);
	}

    /**
     * Logout of CAS by route
     */
    public function getCasLogout ()
    {
        if ( Cas::isAuthenticated() )
        {
            Cas::logout(array('url'=>URL::to($this->redirectTo)));
        } else {
            return redirect($this->loginPath);
        }
    }

    /**
     * Send user to the CAS Authentication page
     */
    public function getCasLogin () 
    {
        // request authentication with server
        Cas::authenticate();

        if ( Cas::isAuthenticated() )
        {
            // Get info from CAS
            $user = Cas::user();
            $attributes = Cas::getAttributes();

            // prepare the info from MySCAD
            $info = array(
                'scad_id'       => $attributes['cn'],
                'email'         => $attributes['EmailAddress'],
                'first_name'    => $attributes['Formatted Name'],
                'last_name'     => $attributes['Last Name']
            );

            return $this->processLogin( $info );
        } else {
            // Needs Authentication first
            return redirect($this->loginPath())
                        ->withErrors([
                            'credentials' => 'Please use the Sign In button to sign in to MySCAD first.'
                        ]);
        }

        /*// if we've returned, ask for user
        $user = Cas::user();

        if ( $user )
        {
            // Get the user details
            $attributes = Cas::getAttributes();

            // Proceed to log them into to Trace
            return $this->processLogin( $attributes );
        } else {
            // Create a new user from the details
            return $attributes;
        }*/

        /**
         * JSON results:
         * {
         *     "cn": "000650331",
         *     "EmailAddress": "JForby@gmail.com",
         *     "Formatted Name": "Jonathan",
         *     "Last Name": "Forby"
         * }
         */
    }

    /**
     * Make a new user from CAS request
     */
    public function processNewUser ( $info )
    {
        // Create a new user
        $user = User::create( $info );

        // Get list of approved admin IDs
        $admins = Config::get('users')['admins'];

        // Default User Role
        $user->attachRole(3);

        // Check if this new user should be an admin
        // Otherwise make them a user
        if ( in_array($info['scad_id'], $admins) ) {
            // attach admin role
            $user->attachRole(1);

            // and editing role
            $user->attachRole(2);
        }

        // Trigger flash message for getting started.
        $url = url('user', $user->slug);
        flash()->success('Welcome to Trace! How about <a href="'.$url.'">checking out your profile</a> and making it your own.');

        // Log them in
        return $this->forceLogin( $user );
    }

    /**
     * Process a login request from CAS
     */
    public function processLogin ( $info )
    {
        // Do we have an account for that user?
        $user = User::findByScadId( $info['scad_id'] );

        if ( $user ) {
            // Patch any new user info from MySCAD to Trace
            $user->update( $info );

            // Get admins list
            $admins = Config::get('users')['admins'];

            // Should they be an admin?
            if ( in_array( $info['scad_id'], $admins ) && !$user->hasRole('admin') ) {
                // Make admin
                $user->attachRole(1);

                // and editing role
                $user->attachRole(2);
            }

            // Log in
            return $this->forceLogin( $user );
        } else {
            // Create a new user
            return $this->processNewUser( $info );
        }
    }

    /**
     * Login User
     */
    public function forceLogin ( User $user )
    {
        // Log them in
        Auth:: login( $user );

        // Go home
        return redirect('/');
    }

    /**
     * Override logout procedure to include CAS
     */
    /*public function getLogout()
    {
       // logout
       $this->auth->logout();
        
        // Logout of CAS
        if ( Cas::isAuthenticated() )
        {
            Cas::logout(array('url'=>URL::to('/')));
        } else {
            return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
        }
    }*/

	/**
	 * Confirm a user's email address
	 * @param  String $confirmation_code 
	 * @return mixed    
	 */
	public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            // throw new InvalidConfirmationCodeException;
            App::abort(404, 'Whoops, looks like that code is invalid');
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if ( ! $user)
        {
            // throw new InvalidConfirmationCodeException;
            App::abort(404, 'Whoops, looks like that user no longer exists');
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        // Log the user in
        $this->auth->login( $user );

       flash()->success('There you are! Welcome to Trace!');

        return redirect($this->redirectPath());
    }

    /**
     * Display a message to the user after confirmation
     */
    public function wait()
    {
        // Get name from session
        $name = Session::get('name');

        // Show view
        return view('auth.wait', compact('name'));
    }

}
