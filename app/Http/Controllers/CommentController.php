<?php namespace App\Http\Controllers;

use Auth;
use Mail;
use Response;
use App\Comment;
use App\Article;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function store( Request $request )
    {
        // Validate the message
        // This will automatically return error JSON if it fails
        $this->validate(
            $request, 
            [
                'message' => 'required|min:2'
            ], 
            [
                'message.required' => 'Oops, we need a message.'
            ]
        );

        $user = Auth::user();
        
        // Otherwise, let's continue...
        // Create the Comment and fill the needed IDs
        $comment = Comment::create( array(
            'message' => $request->message,
            'user_id' => $user->id,
            'article_id' => $request->article_id
        ) );

        // Get the Article title and slug
        $article = Article::find($request->article_id);

        // Get the article author
        $author = $article->user;

        // Send an email to the author
        // if they are not the commenter
        // and only if they want emails
        if ( ($user->id !== $author->id) && ($author->email_notify)  ) {
            if ( $author->email ) {
                Mail::send('emails.comment', compact('article', 'comment'), function( $message ) use ( $author ) {
                    $message->to($author->email, $author->name);
                    $message->subject('A shiny new comment');
                });
            }
        }

        // Grab the index of the new comment
        $commentAnchor = $comment->id;

        // Get the class
        $class = $comment->commentClasses;

        // Template needs currentUser
        $currentUser = $user;

        // This is ajax, but return the comment
        // template for the frontend JS to use.
        return view('partials.articles.comment', compact('comment', 'user', 'currentUser', 'class', 'commentAnchor'));
    }

    /**
     * Remove a Comment
     */
    public function destroy( $id )
    {
        // Who are you?
        $user = Auth::user();

        // Get the comment
        $comment = Comment::find( $id );
        
        // Can you do this?
        if ( $user->can('moderate-comment') || $user->id == $comment->user->id ) {
            //YES
            
            // Delete the comment
            $comment->delete();

            // Respond
            return Response::json(['Success'], 200);
        } else {
            // No, Respond
            return Response::json('Error', 400);
        }

    }

}
