<?php namespace App\Http\Controllers;

use File;
use Request;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ImageController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function upload()
	{
		// Get file, filename, and destination
		$file = Request::file('files')[0];
		$filename = $file->getClientOriginalName();
		$size = $file->getClientSize();
		$destination = public_path() . '/uploads/';
		$url = asset('uploads/' . $filename);

		$response = array(
			'files' => array(
				array(
					'name' => $filename,
					'size' => $size,
				)
			)
		);

		// Try
		$upload_success = $file->move( $destination, $filename );

		if ( $upload_success ) {
			// add url
            $response['files'][0]['url'] = $url;
        } else {
        	// add error
            $response['files'][0]['error'] = 'Upload Failed';
        }

        return Response::json( $response );
	}

	public function delete()
	{
		$url = Request::get('file');
		$filepath = public_path() . '/uploads/' . basename( $url );

		$delete_success = File::delete( $filepath );

		if ( $delete_success )
		{
			return Response::json('success', 200);
		} else {
			return Response::json('error', 400);
		}
	}

}
