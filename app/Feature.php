<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model {

	   protected $fillable = [
            'title',
            'description',
            'button_text',
            'photo',
            'url',
            'active'
        ];

        public function scopeDefault( $query )
        {
            $query->where('default', true);
        }

        public function scopeActive( $query )
        {
            $query->where('default', false);
        }

}
