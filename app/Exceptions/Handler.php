<?php namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
use Illuminate\Session\TokenMismatchException as TokenMismatchException;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		// Custom Error Views:
		/*if ($e instanceof CustomException) {
	        return response()->view('errors.custom', [], 500);
	    }*/

	    // Catch Model Not Found
	    if ($e instanceof ModelNotFoundException) {
	        return response()->view('errors.404', [], 404);
	    }

	    // Catch Wrong CSRF
	    if ($e instanceof TokenMismatchException) {
	        return response()->view('errors.token', [], 500);
	    }

		return parent::render($request, $e);
	}

}
