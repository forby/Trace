<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

	protected $fillable = [
        'message',
        'user_id',
        'article_id'
    ];

    public function article()
    {
        return $this->belongsTo('App\Article');
    }

    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }

    /**
     * Find out if logged in user is the comment author
     */
    public function getIsCurrentUserAttribute()
    {
        return $this->user == $this->article->user;
    }

    /**
     * Get the classes based on author or not
     */
    public function getCommentClassesAttribute()
    {
        if ( $this->isCurrentUser ) {
            return 'comment author';
        } else {
            return 'comment';
        }
    }

}
