<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;

class Category extends Model implements SluggableInterface {

    use SluggableTrait;

    protected $table = 'categories';

	protected $fillable =
    [
        'name'
    ];

    protected $sluggable = array(
        'build_from' => 'name',
        'save_to'    => 'slug'
    );

    /**
     * Get all articles in this category
     */
    public function articles()
    {
        return $this->hasMany('\App\Article');
    }

    /**
     * Get the number of articles in this category
     */
    public function getCountAttribute() {
        return $this->articles()->published()->count();
    }

}
