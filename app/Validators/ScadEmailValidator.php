<?php namespace App\Validators;

use Illuminate\Validation\Validator;

/**
* Custom Validator to make sure Email is from SCAD
*/
class ScadEmailValidator extends Validator
{
	
	public function validateScadEmail($attribute, $value, $parameters)
    {
    	return preg_match( "/^[a-z0-9@\.\+]*scad\.edu$/mi", $value );
    }
}