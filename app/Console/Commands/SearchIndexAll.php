<?php namespace App\Console\Commands;

use App\Tag;
use App\User;
use App\Article;
use Illuminate\Console\Command;

class SearchIndexAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'algolia:indexall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update search indexes for Algolia';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Update Users
        User::reindex();
        $this->info('Users indexed.');

        // Update Articles
        Article::reindex();
        $this->info('Articles reindexed.');

        // Update Tags
        Tag::reindex();
        $this->info('Tags reindexed.');
    }
}
