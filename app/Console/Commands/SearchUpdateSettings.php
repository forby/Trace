<?php namespace App\Console\Commands;

use App\Tag;
use App\User;
use App\Article;
use Illuminate\Console\Command;

class SearchUpdateSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'algolia:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update search index settings for Algolia';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Update Users
        User::setSettings();
        $this->info('User settings updated.');

        // Update Articles
        Article::setSettings();
        $this->info('Article settings updated.');

        // Update Tags
        Tag::setSettings();
        $this->info('Tag settings updated.');
    }
}
