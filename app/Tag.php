<?php namespace App;

use App\Tag;
use Illuminate\Database\Eloquent\Model;
use AlgoliaSearch\Laravel\AlgoliaEloquentTrait;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;

class Tag extends Model implements SluggableInterface {

    use SluggableTrait, AlgoliaEloquentTrait;

	protected $fillable =
	[
		'name'
	];

    protected $sluggable = array(
        'build_from' => 'name',
        'save_to'    => 'slug'
    );

    /**
     * Attributes for Algolia Search
     *
     * @var  array
     */
    public $algoliaSettings = [
        'attributesToIndex' => [
            'name'
        ]
    ];

    public function getAlgoliaRecord()
    {
        // Inject items into search record
        return array_merge($this->toArray(), [
            'url'   => '/tag/' . $this->slug,
            'published' => $this->publishedArticleCount
        ]);
    }

    /**
     * Get all articles that have this tag
     */
	public function articles()
	{
		return $this->belongsToMany('\App\Article')->withTimestamps();
	}

    /**
     * Get amount of articles in tag
     */
    public function getArticleCountAttribute()
    {
        return $this->articles()->count();
    }

    /**
     * Get amount of publihshed articles in tag
     */
    public function getPublishedArticleCountAttribute()
    {
        return $this->articles()->published()->count();
    }

    /**
     * Get tags ordered by article count
     */
    public static function getTopTags()
    {
        return Tag::all()->sortByDesc( function( $model )
        {
            return $model->article_count;
        });
    }

}
