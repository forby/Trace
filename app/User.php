<?php namespace App;

use App\Article;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use AlgoliaSearch\Laravel\AlgoliaEloquentTrait;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, SluggableInterface {

	use Authenticatable, CanResetPassword, EntrustUserTrait, SluggableTrait, SoftDeletes, AlgoliaEloquentTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * Settings for generated slug
	 */
	protected $sluggable = array(
        'build_from' => 'name',
        'on_update'  => true
    );

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'first_name', 
		'last_name',
		'scad_id',
		'email',
		'password',
		'avatar',
		'avatar_index',
		'avatar_default',
		'pro_title',
		'hometown',
		'major',
		'experience',
		'twitter',
		'instagram',
		'linkedin',
		'public_email',
		'facebook',
		'website',
		'custom_name'
	];

	/**
	 * Attributes for Algolia Search
	 *
	 * @var  array
	 */
	public $algoliaSettings = [
        'attributesToIndex' => [
            'name',
            'hometown'
        ]
    ];

    public function getAlgoliaRecord()
    {
    	// Inject name into search record
        return array_merge($this->toArray(), [
            'name' => $this->name,
            'url'	=> '/user/' . $this->slug
        ]);
    }

	/**
	 * Make sure these return as dates
	 * @var array
	 */
	protected $dates = ['deleted_at'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	 * Get the user's full name or custom name
	 */
	public function getNameAttribute()
	{
		return (!$this->custom_name) ? $this->first_name . ' ' . $this->last_name : $this->custom_name;
	}

	/**
	 * Extend the create function
	 */
	public static function create(array $attributes = [])
	{
		// add a random user profile image
		$index = str_pad( rand(1, 10) , 2, '0', STR_PAD_LEFT);
		$attributes['avatar_default'] = '/img/emoji/face-' . $index . '.svg';

		// manually set password for now
		$attributes['password'] = bcrypt('toothbrush');
		
		// run default create
		return parent::create( $attributes );
	}

	/**
	 * Get count of this user's articles
	 * that have been super bumped
	 */
	public function getSuperCountAttribute()
	{
		// Get the current list of super bumps
		$super_bumps = Article::with('user')->superBumped()->take(20)->get();

		// Get this users bumped articles
		$articles = $this->bumped->get();

		// How many are in the collection with this user's ID?
		return $super_bumps->where('user_id', $this->id)->count();
	}

	/**
	 * Get this user's articles
	 * 
	 * @return collection
	 */
	public function articles()
	{
		return $this->hasMany('App\Article')->whereNull('articles.deleted_at');
	}

	/**
	 * Get this user's comments
	 * 
	 * @return collection
	 */
	public function comments()
	{
		return $this->hasMany('App\Comment');
	}

	/**
	 * Get articles this user has commented on
	 * 
	 * @return collection
	 */
	public function getCommentedAttribute()
	{
		/*
			// Actual workign query:

			select articles.id, comments.message, comments.user_id from `articles` 
			inner join `comments` 
			on `articles`.`id` = `comments`.`article_id`
			and `comments`.`user_id` = 1
			where ( articles.id, comments.created_at ) in
			(
				select c.article_id, max(c.created_at)
				from comments c
				group by c.article_id
			)
			order by comments.created_at desc

		 */

		$user_id = $this->id;

		return Article::published()->join('comments', function( $join ) use ( $user_id ) {
			$join->on('articles.id', '=', 'comments.article_id')
				->where('comments.user_id', '=', $user_id);
		})
		->select('comments.id as comment_id', 'articles.*')
		->whereRaw('(`articles`.`id`, `comments`.`created_at`) in (select comments.article_id, max(comments.created_at) from `comments` group by `article_id`)')
		->orderBy('comments.created_at', 'desc');
	}

	/**
	 * Get the count of articles this user has commented on
	 */
	public function getCommentedCountAttribute()
	{
		return $this->commented->count();
	}

	/**
	 * Get articles this user has bumped
	 */
	public function getBumpedAttribute()
	{
		return Article::published()->whereLiked( $this->id );
	}

	/**
	 * Get number of articles this user has bumped
	 */
	public function getBumpedCountAttribute()
	{
		// instead use Article::published()->likeCount? 
		// maybe same thing
		return $this->bumped->count();
	}

	/**
	 * Make sure avatar returns something useful
	 */
	public function getAvatarImageAttribute()
	{
		if ( ! $this->avatar ) {
			return $this->avatar_default;
		}

		return $this->avatar;
	}

	/**
	 * Get the email notification setting status
	 */
	public function getNotificationStatusAttribute()
	{
		return $this->email_notify;
	}

	/**
	 * Query scope for finding user by SCAD ID
	 */
	public function scopeWhereScadId ( $query, $id )
	{
		return $query
			->where('scad_id', $id)
			->orWhere('scad_id', ltrim( $id, '0' ))
			->orWhere('scad_id', str_pad( $id, 9, '0', STR_PAD_LEFT ));
	}

	/**
	 * Accessor for finding by scad id
	 */
	public static function findByScadId ( $id )
	{
		return self::whereScadId( $id )->first();
	}

}
