var elixir = require('laravel-elixir');
var dotenv = require('dotenv').load();

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

// Define some paths
var components = './vendor/bower_components/';
var paths = {
	'jquery'            :'jquery/',
	'bootstrap'         :'bootstrap-sass/assets/',
	'selectize'         :'selectize/dist/',
    'medium'            :'medium-editor/dist/',
    'handlebars'        :'handlebars/',
    'jquery_sortable'   :'jquery-sortable/source/',
    'blueimp_upload'    :'blueimp-file-upload/',
    'font_awesome'      :'font-awesome/',
    'typeahead'         :'typeahead.js/dist/',
    'bourbon'           :'bourbon/app/assets/stylesheets/',
    'neat'              :'neat/app/assets/stylesheets/',
    'bitters'           :'bitters/app/assets/stylesheets/',
    'normalize'         :'normalize-css/',
    'autosize'          :'jquery-autosize/dist/',
    'algolia'           :'algoliasearch/dist/',
}

elixir(function(mix) {

    // Combine all SCSS and CSS
    // use includePaths to specify sources for imports within styles.scss
    mix.sass(
        [
            './public/css/fonts.css',
            components + paths.normalize + 'normalize.css',
            'styles.scss',
            components + paths.selectize + 'css/selectize.css',
            components + paths.medium + 'css/medium-editor.css',
            components + paths.medium + 'css/themes/default.css',
            components + paths.font_awesome + 'css/font-awesome.css',
        ], 
        'public/css/', 
        {includePaths: [
        	// components + paths.bootstrap + 'stylesheets/',
            components + paths.bourbon,
            components + paths.neat,
            components + paths.bitters
        ]}
    );
    
    // copy over bootstrap fonts
    // mix.copy(components + paths.bootstrap + 'fonts/bootstrap/**', 'public/fonts');
    mix.copy(components + paths.font_awesome + 'fonts/**', 'public/fonts');
    	
    // combine JS files
    mix.scripts([
	    components + paths.jquery + 'dist/jquery.js',
        './public/js/vendor/jquery.mobile.custom.min.js',
	    // components + paths.bootstrap + 'javascripts/bootstrap.js',
	    components + paths.selectize + 'js/standalone/selectize.js',
        components + paths.medium + 'js/medium-editor.js',
        components + paths.handlebars + 'handlebars.min.js',
        // components + paths.jquery_sortable + 'js/jquery-sortable-min.js',
        // components + paths.blueimp_upload + 'js/vendor/jquery.ui.widget.js',
        // components + paths.blueimp_upload + 'js/jquery.iframe-transport.js',
        // components + paths.blueimp_upload + 'js/jquery.fileupload.js',
        // components + paths.medium_insert + 'js/medium-editor-insert-plugin.min.js',
        components + paths.typeahead + 'typeahead.bundle.js',
        components + paths.algolia + 'algoliasearch.min.js',
        components + paths.autosize + 'autosize.js',
        // 'resources/assets/js/menu.js',
        'resources/assets/js/main.js'
	], 'public/js/app.js', './');
		
	// make cache buster
	mix.version(['css/app.css', 'js/app.js']);
});
