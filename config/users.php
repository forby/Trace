<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admins Configuration
    |--------------------------------------------------------------------------
    |
    | Add scad_id's of users who will be added automatically as admins
    |
    */
   
    'admins' => [
        '000650331',        // Jonathan Forby
        '000626498',        // Josh Lind
        '001913761',        // Leigh Britton
        '001805586'         // Lauren Wolf
    ],

    /*
    |--------------------------------------------------------------------------
    | Default Authors
    |--------------------------------------------------------------------------
    |
    | Add the initial author accounts to create in preparation for Trace
    |
    */
    
    'authors' => [
        ["name"=>"Alex Gough","scad_id"=>1094344],
        ["name"=>"Andrea Young","scad_id"=>1586228],
        ["name"=>"Aniela Chertavian","scad_id"=>1551852],
        ["name"=>"Ariel Mael","scad_id"=>1411815],
        ["name"=>"Austin Shaw","scad_id"=>1436650],
        ["name"=>"Chris Lindvall","scad_id"=>1699759],
        ["name"=>"Daniela Vollmer","scad_id"=>1493826],
        ["name"=>"Hannah Moyers","scad_id"=>1441324],
        ["name"=>"Jade Stoner","scad_id"=>1416215],
        ["name"=>"Madison Myers","scad_id"=>1772013],
        ["name"=>"Monica Tisminesky","scad_id"=>871387],
        ["name"=>"Paige Cano","scad_id"=>1409316],
        ["name"=>"Sandra M. Montalvo","scad_id"=>1631468],
        ["name"=>"Stefanie Gomez","scad_id"=>1546849],
        ["name"=>"Andrea Goto","scad_id"=>622467],
        ["name"=>"Anita Akella","scad_id"=>1039593],
        ["name"=>"Ben Bush","scad_id"=>1751057],
        ["name"=>"Catie Pizzichemi","scad_id"=>672310],
        ["name"=>"Chris Williams","scad_id"=>824681],
        ["name"=>"Clark Delashmet","scad_id"=>765155],
        ["name"=>"Jason Fox","scad_id"=>31570],
        ["name"=>"John McCabe","scad_id"=>827329],
        ["name"=>"John Webber","scad_id"=>834743],
        ["name"=>"Josh Jalbert","scad_id"=>1432959],
        ["name"=>"Justin Gunther","scad_id"=>648723],
        ["name"=>"Kelly Theisen","scad_id"=>1711887],
        ["name"=>"Mary Kim","scad_id"=>756606],
        ["name"=>"Jamie Gall","scad_id"=>877068],
        ["name"=>"Josh Lind","scad_id"=>626498],
        ["name"=>"Michael Chaney","scad_id"=>11734],
        ["name"=>"Michelle Quick","scad_id"=>1537984],
        ["name"=>"Rebecca Weldon","scad_id"=>828738],
        ["name"=>"Rick Lovell","scad_id"=>799370],
        ["name"=>"Robert Brown","scad_id"=>661176],
        ["name"=>"Scott Boylston","scad_id"=>600174],
        ["name"=>"Sheila Edwards","scad_id"=>29101],
        ["name"=>"SuAnne Fu","scad_id"=>671064],
        ["name"=>"Tom Hardy","scad_id"=>1437871],
        ["name"=>"Xenia Viladas","scad_id"=>1775392],
        ["name"=>"Jonathan Furby","scad_id"=>650331]
    ],
   
];