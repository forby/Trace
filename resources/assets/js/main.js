$(function() {

    var desktopWidth = 1030;

    // Preload done
    $('body').removeClass('preload');

    // Dismiss modal boxes
    $('div.alert .close').click(function(e) {
        e.preventDefault();
        $(this).closest('.alert').remove();
    });

    // move nav element position according to window width
    moveNavigation();
    $(window).on('resize', function(){
        (!window.requestAnimationFrame) ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation);

        // right nav positioning
        updateNavPadding();
    });

    // Override link clicks with target '#'
    $('a[href="#"]').click(function(e){
        e.preventDefault();
    });

    // String trunc function
    String.prototype.trunc =
    function(n,useWordBoundary){
        var toLong = this.length>n,
        s_ = toLong ? this.substr(0,n-1) : this;
        s_ = useWordBoundary && toLong ? s_.substr(0,s_.lastIndexOf(' ')) : s_;
        return  toLong ? s_ + '...' : s_;
    };

    // Visibility toggle
    // Allow elements to switch visibility to another element
    $('.vis-toggle').click(function(e) {
        e.preventDefault();
        
        $( $(this).data('show') ).removeClass('is-hidden');
        $( $(this).data('hide') ).addClass('is-hidden');
    });

    updateNavPadding();
    function updateNavPadding() {
        var el = $('.right-nav'),
        padding = 35;
        if ( el.length == 0 ) return;
        
        if ( el.parent().parent().hasClass('header-wrap') ) {
            el.css('padding-right', $('.header-wrap').offset().left + $('.header-buttons').outerWidth() + padding);
        } else {
            el.css('padding-right', '');
        }
    }

    // Window Width Check
    function checkWindowWidth() {
        //check window width (scrollbar included)
        var e = window, 
        a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        if ( e[ a+'Width' ] >= desktopWidth ) {
            return true;
        } else {
            return false;
        }
    }

    // Switch Nav in the Dom
    function moveNavigation(){
        var navigation = $('.main-nav');
        var desktop = checkWindowWidth();
        if ( desktop ) {
            navigation.detach();
            navigation.insertBefore('.header-buttons');
        } else {
            navigation.detach();
            navigation.insertAfter('.main-under-header');
        }
    }

    /**
     * Search Button
     */

     var searchStatus = false;

     function toggleSearch(type) {
        if(type=="close") {
            searchStatus = false;
            $('#search-modal').addClass('is-hidden');
            //$('.overlay').removeClass('is-visible');
            $('#search-field').addClass('can-close').trigger('typeahead:close').removeClass('can-close');

        } else {
            //toggle search visibility
            $('#search-modal').toggleClass('is-hidden');
            if ( $('#search-modal').hasClass('is-visible') ) {
                searchStatus = false;
            } else {
                //$('.overlay').removeClass('is-visibile');
                $('#search-field').val('').focus();
                searchStatus = true;
            }
        }
    }

    /**
    * Live search
    */

    // Try to launch search when typing
    window.onkeydown = function (e) {
        if (!e) e = window.event;
        if (!e.metaKey) {
            if(e.keyCode >= 65 && event.keyCode <= 90 || e.keyCode >= 48 && event.keyCode <= 57) {
                if (!searchStatus) {
                    // Make sure we aren't focussed on something else
                    var activeElement = document.activeElement;
                    var body = document.getElementsByTagName('body')[0];
                    if ( activeElement === body ) {
                        toggleSearch();
                    }
                }
            }
        }
    }

    // Close when hitting ESC
    $(document).keyup(function(e) {
        if (e.keyCode == 27 && searchStatus) {
            toggleSearch('close');
        }
    });

    // Prevent these inputs from triggering the search
    $('.form-control, .selectize-input input').keydown(function(e) {
        e.stopPropagation();
    });

    $('.search-trigger').on('click', function(e) {
        toggleSearch();
        closeNav();
        e.preventDefault();
    });

     // close search from modal
     $('#close-search').on('click', function(e) {
        toggleSearch('close');        
        e.preventDefault();
    });

     // profile menu
     $('.profile-trigger').click(function(e) {
        e.preventDefault();
        e.stopPropagation();

        var el = $(this);
        var menu = el.next();
        
        if ( menu.hasClass('is-visible') ) {
            menu.removeClass('is-visible');
        } else {
            e.stopPropagation();
            menu.addClass('is-visible');
            $('html').one('click', function(e) {
                el.trigger('click');
            });
        }
    });

     /**
      * Menu Button
      */
      $('.nav-trigger').on('click', function(e) {
        if ( $('.main-under-header').hasClass('nav-is-visible') ) {
            closeNav();
            $('.overlay').removeClass('is-visible');
        } else {
            openNav(this);
        }

        toggleSearch('close');
        e.preventDefault();
    });

     /**
      * SubMenus
      *
      * prevent default clicking on direct children of .main-nav
      */
      $('.main-nav').children('.has-children').children('a').on('click', function(event){
        event.preventDefault();
    });
     //open submenu
     $('.has-children').children('a').on('click', function(event){
        event.preventDefault();
        var selected = $(this);
        if( selected.next().hasClass('is-hidden') ) {
            //desktop version only
            selected.addClass('selected').next().removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('moves-out');
            selected.parent('.has-children').siblings('.has-children').children('ul').addClass('is-hidden').end().children('a').removeClass('selected');
            $('.overlay').addClass('is-visible');
        } else {
            //console.log('MOBILE');
            selected.removeClass('selected').next().addClass('is-hidden').end().parent('.has-children').parent('ul').removeClass('moves-out');
            $('.overlay').removeClass('is-visible');
        }
    });
     //submenu items - go back link
     $('.go-back').on('click', function(){
        $(this).closest('.secondary-nav').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('moves-out');
    });

     /**
      * Overlay Interactivity
      *
      * Close lateral menu on mobile
      */
      $('.overlay').on('swiperight', function(){
        if($('.main-nav').hasClass('nav-is-visible')) {
            closeNav();
            $('.overlay').removeClass('is-visible');
        }
    });
      $('.nav-on-left .overlay').on('swipeleft', function(){
        if($('.nav-main').hasClass('nav-is-visible')) {
            closeNav();
            $('.overlay').removeClass('is-visible');
        }
    });
      $('.overlay').on('click', function(){
        closeNav();
        toggleSearch('close');
        $('.overlay').removeClass('is-visible');
    });

     /**
      * Functions
      */

      function closeNav() {
        $('.nav-trigger, .main-header, .main-under-header, .main-nav > ul').removeClass('nav-is-visible');
        $('.has-children ul, .has-children .ul-wrap').addClass('is-hidden');
        $('.has-children a').removeClass('selected');
        $('.moves-out').removeClass('moves-out');
    }

    function openNav(el) {
        $(el).addClass('nav-is-visible');
        $('.main-nav > ul, .main-header').addClass('nav-is-visible');
        $('.main-under-header').addClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
            $('body').addClass('overflow-hidden');
        });
        $('.overlay').addClass('is-visible');
    }

    // ==========================
    // TYPEAHEAD SEARCH
    // ==========================

    var client = (this.domain == 'trace.dev') ? algoliasearch('YBVJ8J6UZX', '0afa5bdb3c6356253c8cee519dfe3e53') : algoliasearch('EKWWV34WJF', 'b59a8c4da079ed61275669ffde0b1adc');
    var articles_index = client.initIndex('articles');
    var users_index = client.initIndex('users');
    var tags_index = client.initIndex('tags');
    
    $('#search-field').typeahead({
        highlight: true,
        minLength: 2
    }, {
        name: 'articles',
        display: 'title',
        source: articles_index.ttAdapter({hitsPerPage: 5}),
        templates: {
            header: '<strong>Articles</strong>',
            suggestion: Handlebars.compile('<div><span class="title">{{title}}</span><span class="user">by {{username}}</span></div>')
        }
    }, {
        name: 'users',
        display: 'name',
        source: users_index.ttAdapter({hitsPerPage: 5}),
        templates: {
            header: '<strong>Users</strong>',
            suggestion: Handlebars.compile('<div>{{name}}</div>')
        }
    }, {
        name: 'tags',
        display: 'name',
        source: tags_index.ttAdapter({
            hitsPerPage: 5,
            numericFilters: ["published>0"]
        }),
        templates: {
            header: '<strong>Tags</strong>'
        }
    }).on('typeahead:select', function(e, data) {
        window.location = data.url;
    }).on('typeahead:beforeclose', function(e) {
        // only close menu if we really can
        // toggle search uses can-close flag
        if (!$(this).hasClass('can-close')) {
            e.preventDefault();
            e.stopPropagation();
        }
    });

    // Add CSRF token to all future AJAX requests
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     * Global autosize for certain textarea
     */
     autosize( $('textarea.autosize') );

    /**
     * Star Control for Admins
     */
     $('.star-control').click(function(e) {
        e.preventDefault();

        var star    = $(this);
        var id      = star.data('id');
        var url     =  '/api/article/feature/toggle';

        console.log(url);

        $.post(
            url,
            {
                'id' : id
            },
            function(response) {
                // response is an array of classes
                star.children('.star').removeClass().addClass( response.classes );
            }
            );
    });

    /**
     * Global Modal stuff
     */

    //open popup
    $('.cd-popup-trigger').on('click', function(event){
        event.preventDefault();
        $('.cd-popup').addClass('is-visible');
    });

    // open popup as interruption
    $('.modal-interrupt').on('click', function(e) {
        e.preventDefault();
        var location = $(this).attr('href');
        $('.cd-popup').addClass('is-visible').data('continue', location);
    });

    $('.cd-popup-continue').click(function(e) {
        e.preventDefault();

        var location = $(this).closest('.cd-popup').data('continue');

        if ( location ) {
            window.location = location;
        }
    });
    
    //close popup
    $('.cd-popup').on('click', function(event){
        if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') || $(event.target).is('.cd-popup-dismiss') ) {
            event.preventDefault();
            $(this).removeClass('is-visible');
        }
    });
    
    //close popup when clicking the esc keyboard button
    // $(document).keyup(function(event){
    //     if(event.which=='27'){
    //         $('.cd-popup').removeClass('is-visible');
    //     }
    // });

    // $('#search-field').selectize({
    //  valueField: 'url',
    //  labelField: 'name',
    //  searchField: ['name'],
    //  maxOptions: 10,
    //  options: [],
    //  create: false,
    //  render: {
    //      option: function(item, escape) {
    //          return '<div>' +escape(item.name)+'</div>';
    //      }
    //  },
    //  optgroups: [
    //  {value: 'article', label: 'Articles'},
    //  {value: 'user', label: 'Users'}
    //  ],
    //  optgroupField: 'class',
    //  optgroupOrder: ['article','user'],
    //  load: function(query, callback) {
    //      if (!query.length) return callback();
    //      $.ajax({
    //          url: root+'/api/search/' + query,
    //          type: 'GET',
    //          dataType: 'json',
    //          error: function() {
    //              callback();
    //          },
    //          success: function(res) {
    //              callback(res.data);
    //          }
    //      });
    //  },
    //  onChange: function(){
    //      window.location = this.items[0];
    //  }
    // });
});