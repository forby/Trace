<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "No dice :( Passwords must be at least six characters and match each other.",
	"user"     => "WHO ARE YOU?!? We can't find anyone with that email address.",
	"token"    => "That's weird. This password reset token is invalid.",
	"sent"     => "We have sent your password reset link. CHECK IT!",
	"reset"    => "Aw yeah! Your password has been reset!",

];
