@extends('app')

@section('content')
<div class="container">
	
	{{-- @if (Auth::check())
	<div class="alert alert-success" role="alert">
  		You are successfully logged in!
	</div>
	@endif --}}
	<h1 class="page-title">Testing Editor</h1>
	<p class="lead">Editor:</p>
</div>

<div class="container">
	<div class="first editable"></div>
	<div class="second editable"></div>
</div>

<div class="container">
	<div class="panel panel-default">
		<div class="panel-body">
			<a href="#" id="save" class="btn btn-primary">Save</a>
		</div>
	</div>

</div>
@endsection

@section('page-js')
<script type="text/javascript">
	$(function() {
		var editor = new MediumEditor('.first.editable', {
			placeholder: 'testing'
		});
		var editor = new MediumEditor('.second.editable', {
			placeholder: 'another test'
		});

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': '{{ csrf_token() }}'
			}
		});

		$('.editable').mediumInsert({
			editor: editor,
			addons: {
				images: {
					fileUploadOptions: {
						url: '{{ URL::to('image/upload') }}',
						acceptFileTypes: /(.|\/)(gif|jpe?g|png)$/i,
						maxFileSize: 5000000
					},
					deleteScript: '{{ URL::to('image/delete') }}',
					uploadCompleted: function (el, data) {
						console.log(['UPLOADED!',el, data])
					},
					styles: {
						full: {
							label: '<span class="fa fa-align-justify"></span>',
							added: function ($el) {
								$el.addClass('medium-insert-images-full container-fluid');

								// wrap everything before and after in a container
								$el.prevAll().wrap('<div class="container"></div>');
								$el.nextAll().wrap('<div class="container"></div>');

								// remove the outer container
								$el.parent().children().unwrap();
							},
							removed: function ($el) {
								$el.removeClass('medium-insert-images-full container-fluid');
							},
						},
						wide: {
							label: '<span class="fa fa-align-justify"></span>'
						},
						left: {
							label: '<span class="fa fa-align-left"></span>'
						},
						right: {
							label: '<span class="fa fa-align-right"></span>'
						},
						grid: {
							label: '<span class="fa fa-th"></span>'
						}
					},
				}
			},
		});

$('#save').click(function(e) {
	e.preventDefault();

	console.log('click');
	console.log( editor.serialize() );
});
});
</script>
@endsection