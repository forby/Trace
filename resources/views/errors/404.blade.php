@extends('app')

@section('content')
    <div class="container thin head-space foot-space">
        <h1 class="page-title">WHOOPS - We couldn't find that page.</h1>
        <p class="large">And we're looking <em>pretty</em> hard. Are you in the right place?</p>
        <p>Error code: 404</p>
    </div>
@endsection