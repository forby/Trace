@extends('app')

@section('content')
    <div class="container thin head-space foot-space">
        <h1 class="page-title">Something's not right.</h1>
        <p class="large">We can't fulfil that request right now.</p>
        <p>Error code: 502</p>
    </div>
@endsection