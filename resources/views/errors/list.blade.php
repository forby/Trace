@if ( $errors->any() )
	<ul class="list-unstyled errors-list">
		@foreach ( $errors->all() as $error )
			<li><span class="icon icon-lightning"></span>{{ $error }}</li>
		@endforeach
	</ul>
@endif