@extends('app')

@section('content')
    <div class="container thin head-space foot-space">
        <h1 class="page-title">What?!</h1>
        <p class="large">Something's not right.  Either you've logged out or your cookie has expired.  Can you try that again?</p>
        <p>Error: Invalid Security Token</p>
    </div>
@endsection