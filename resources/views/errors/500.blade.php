@extends('app')

@section('content')
    <div class="container thin head-space foot-space">
        <h1 class="page-title">Ouch!</h1>
        <p class="large">Something has gone horribly wrong. We're working on it. Please try again soon.</p>
        <p>Error code: 500</p>
    </div>
@endsection