@extends('app')

@section('content')
    <div class="container thin head-space foot-space">
        <h1 class="page-title">Not good....</h1>
        <p class="large">Looks like that confirmation code has already been used.</p>
    </div>
@endsection