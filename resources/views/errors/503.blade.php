@extends('app')

@section('content')
    <div class="container thin head-space foot-space">
        <h1 class="page-title">Be right back.</h1>
        <p class="large">Looks like Trace is unavailable at the moment.  We'll be back! Stay tuned!</p>
        <p>Error code: 503</p>
    </div>
@endsection