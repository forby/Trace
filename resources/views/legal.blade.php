@extends('app')

@section('content')
    <div class="container thin head-space foot-space">
        
        <h1>Privacy policy</h1>

        <p>Welcome to the Web site of the Savannah College of Art and Design. We have created this Privacy Policy to outline our firm commitment to your privacy. Please read this policy to understand how the university treats any personal information you provide via this site. <em>Note: This policy is subject to change.</em></p>
        
        <h3>Information collection and use</h3>

        <p>SCAD is the sole owner of information collected via this Web site. The university does not sell, share or rent such information in ways different from those outlined in this Privacy Policy.</p>
        
        <p>Information about visitors to this site may be collected in the form of the following:</p>
        
        <h4>Applications</h4>
        
        <p>Prospective students applying online voluntarily provide personal contact information (e.g. name and mailing address). Information submitted is used by the admission department to process applications and contact applicants.</p>
        
        <h4>Registration forms</h4>
        
        <p>Many of the events featured throughout the site include online registration forms or forms that may be used to request information, pledge to donate to the university, or otherwise voluntarily submit personal information relating to a specific event. The information in these forms is submitted directly to the department indicated in the event feature and is used by that department only for the purposes indicated to the user when that information is submitted.</p>
        
        <h4>Web survey</h4>
        
        <p>Information submitted in our Web survey is used by the university's communications department to improve this Web site. Inquiries submitted as part of a Web survey may also be forwarded to specific departments best able to address those inquiries.</p>
        
        <h4>Cookies</h4>
        
        <p>A cookie is a piece of data stored on a user's hard drive that contains information about that user. Usage of a cookie does not link to any personally identifiable information. A user rejecting a cookie may still use this Web site. Cookies serve to track and target the interests of our users to improve this Web site.</p>
        
        <h4>Log files</h4>
        
        <p>IP addresses also enable the university to analyze trends, administer this site, track users' movements and gather broad demographic information for aggregate use. IP addresses are not linked to personally identifiable information.</p>
        
        <h3>Sharing</h3>
        <p>The university does not share aggregated demographic information with outside organizations or businesses.</p>
        
        <h3>Links</h3>
        
        <p>This Web site contains links to other Web sites. SCAD is not responsible for privacy practices of other sites; the Privacy Policy you are reading now applies only to this Web site.</p>
        
        <p>Some of the university's business partners use cookies on our Web site (e.g. application and university search services). (Usage of a cookie does not link to any personally identifiable information.) The university has no access to or control over these cookies. To learn more about the practices of our business partners, please view their privacy policies.</p>
        
        <h3>Security</h3>
        
        <p>The university takes every precaution to protect users' information. When a user voluntarily submits information via this Web site, that information is protected.</p>
        
        <p>All user information is restricted to use for the purpose(s) indicated to the user when that information is submitted. Only employees who need the information to perform a specific task (e.g. an admission representative processing an application) are granted access to personally identifiable information.</p>
        
        <p>All employees are informed of the importance of, and any changes in, security and privacy practices as part of their training and in quarterly updates.</p>
        
        <p>Collected information is used for the purposes described in this Privacy Policy and thereafter filed in secure storage or discarded.</p>
        
        <h3>Guidelines for children</h3>
        
        <p>Children under age 13 are asked not to submit any information via this site.</p>
        
        <h3>Consent and user terms</h3>
        
        <p>By accessing this Web site, you consent to the collection, use and storage of your information by SCAD in the manner described in this policy. In addition, you agree to and are bound to abide by the following terms and conditions:</p>
        
        <h4>Copyright policy</h4>
        <p>All images, text, programs, and other materials found in the SCAD Web site are protected by the United States copyright laws. Published information cannot be used in any manner prohibited by law or disallowed by licenses, contracts, copyrights, copyrighted material or college regulation. Any commercial use of the images, text, programs or other materials found in the SCAD Web site is strictly prohibited without the express written consent of SCAD.</p>
        
        <h4>Content submission</h4>
        
        <p>Certain areas of the SCAD Web site allow for the exchange of information between you, the USER and SCAD. Any submission by the USER becomes the property of the university, and the USER implicitly grants the university the authority and right to use that content in accordance with this Privacy Policy.</p>
        
        <h4>User responsibility</h4>
        
        <p>The USER assumes all responsibility for use of the SCAD Web site. The USER waives all claims against the university, its officers, directors, employees, suppliers and programmers that may arise from the utilization of this Web site.</p>
        
        <h4>Accuracy of information</h4>
    
        <p>While all reasonable attempts are made to ensure the accuracy of the information, neither SCAD, nor its information contributors, can be held responsible by the USER for the accuracy of the information found in this Web site. The university makes no expressed or implied warranty as to the accuracy of content or programming. The SCAD Web site does not constitute a contract, and all portions are subject to change without notice.</p>
        
        <h4>Use of photographs</h4>
        
        <p>Representatives of the university take photographs for use in print and electronic publications. This paragraph serves as public notice of the intent of the Savannah College of Art and Design to do so and as a release to the university giving permission to use such images as it deems fit. If you object to the use of your photograph, you have the right to withhold its release by contacting the communications department at 912.525.5225.</p>
        
        <h4>Blog Comments Guidelines</h4>
        
        <p>Comments will be moderated for relevancy, so please stay on topic. We reserve the right to remove posts that are offensive or that detract from a constructive conversation. Thanks for visiting!</p>
        
        <h3>Additional information</h3>
       
        <p>If you have any questions about this policy or Web site, email <a href="mailto:satellite@scad.edu">SCAD's interactive services department</a> or contact the communications department:</p>
        
        <p>Savannah College of Art and Design<br />
            Communications Department<br />
            P.O. Box 3146<br />
            Savannah, GA 31402-3146<br />
            912.525.5225</p>

    </div>
@endsection