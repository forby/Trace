@extends('app')

@section('content')

<div class="showcase{{ !$feature->default ? ' custom' : '' }}">
	<div class="showcase-content">
		<h1>{{ $feature->title }}</h1>
		<p>{{ $feature->description }}</p>
		<a href="{{ url( $feature->url ) }}" class="button">{{ $feature->button_text }}</a>
	</div>
	<div class="background" style="background-image: url('{{ asset( $feature->photo ) }}');"></div>
</div>

<div class="container home-content has-sidebar">
	
	{{-- @if (Auth::check())
	<div class="alert alert-success" role="alert">
  		You are successfully logged in!
	</div>
	@endif --}}
	<section>
		@if ( isset($feature_article) )
			<div class="featured-article">
				@include('partials.articles.verticalFeature', ['article' => $feature_article])	
			</div>
		@endif

		<div class="home-articles">
			<h1>Most Recent</h1>
			@forelse( $articles as $article )
				@include('partials.articles.horizontalMedium', ['article' => $article])
			@empty
				<p>There are no articles, <a href="{{ url('articles/create') }}">go write something</a>.</p>
			@endforelse
		</div>

		@if ( $articles->total() > 5 )
			<div class="pagination">
			    <div class="next">
		            <a href="{{ url('articles') }}">View Archive &rarr;</a>
			    </div>
			</div>
		@endif
	</section>
	<aside class="sidebar">

		@include('sidebar.top_articles', ['articles' => $top_articles])

		@include('sidebar.popular_tags', ['tags' => $top_tags])
		
	</aside>
		
</div>
@endsection
