@extends('emails.template')

@section('body')

    @include('emails.partials.h1', ['content' => 'Time for a new password.'])

    @include('emails.partials.p', ['content' => "You're all set to reset.  What are you waiting for?"])

    @include('emails.partials.button', ['content' => 'Reset your password now', 'url' => url('password/reset/'.$token) ])

    @include('emails.partials.p', ['content' => "Thanks!"])

    @include('emails.partials.p', ['content' => "- Your friends at Trace"])

@endsection