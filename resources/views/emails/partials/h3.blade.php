<h3 style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; font-size: 21px; line-height: 1.2; color: #404041; font-weight: 200; margin: 40px 0 10px; padding: 0;">
    {{ $content }}
</h3>