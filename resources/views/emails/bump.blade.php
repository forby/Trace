@extends('emails.template')

@section('body')

    @include('emails.partials.h1', ['content' => 'AW YEAH!'])

    @include('emails.partials.p-open')
        You just got a new bump on your article, {{ $article->title }}.
    @include('emails.partials.p-close')

    @include('emails.partials.p-open')
    <br>
    @include('emails.partials.p-close')

    @include('emails.partials.button', ['content' => 'Go to your article', 'url' => url('article/'.$article->slug) ])

    @include('emails.partials.p-open')
    <br>
    @include('emails.partials.p-close')

    @include('emails.partials.p', ['content' => "Thanks!"])

    @include('emails.partials.p', ['content' => "- Your friends at Trace"])

@endsection