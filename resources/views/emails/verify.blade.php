@extends('emails.template')

@section('body')

    @include('emails.partials.h1', ['content' => 'Hey friend,'])

    @include('emails.partials.p', ['content' => 'Welcome to Trace, a new space for you share your know-how with fellow collaborators'])

    @include('emails.partials.p', ['content' => 'You\'re almost done.  We just need you to confirm your email address and you\'re all set!'])

    @include('emails.partials.p', ['content' => ' '])

    @include('emails.partials.button', ['content' => 'Confirm Now!', 'url' => url( 'register/verify/' . $confirmation_code )])

@endsection