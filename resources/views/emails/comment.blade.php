@extends('emails.template')

@section('body')

    @include('emails.partials.h1', ['content' => 'Did you hear that?'])

    @include('emails.partials.p', ['content' => 'Someone just commented on your article:'])

    @include('emails.partials.h2-open')
        {{ $article->title }}
    @include('emails.partials.h2-close')

    @include('emails.partials.p-open')
        {{ $comment->message }}<br>- {{ $comment->user->name }}
    @include('emails.partials.p-close')

   @include('emails.partials.p-open')
   <br>
   @include('emails.partials.p-close')

    @include('emails.partials.button', ['content' => 'Go read it on Trace', 'url' => url('article/'.$article->slug.'#'.$comment->id) ])

    @include('emails.partials.p-open')
    <br>
    @include('emails.partials.p-close')

    @include('emails.partials.p', ['content' => "Thanks!"])

    @include('emails.partials.p', ['content' => "- Your friends at Trace"])

@endsection