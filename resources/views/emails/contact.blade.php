@extends('emails.template')

@section('body')

    @include('emails.partials.h1', ['content' => 'You have a new question or comment:'])

    @include('emails.partials.p-open')
        <strong>From:</strong> {{ $name }} ({{ $email }})
    @include('emails.partials.p-close')

    @include('emails.partials.p-open')
        <strong>Subject:</strong> {{ $subject }}
    @include('emails.partials.p-close')

    @include('emails.partials.p-open')
        <strong>Message:</strong><br>{{ $body }}
        <br><br>
    @include('emails.partials.p-close')

    {{-- @include('emails.partials.p', ['content' => "<strong>From:</strong><br>{{ $name }} ({{ $email }})"]) --}}
    {{-- @include('emails.partials.p', ['content' => HTML::decode(<strong>Subject:</strong><br>{{ $subject }})]) --}}
    {{-- @include('emails.partials.p', ['content' => "<strong>Message:</strong><br>{{ $body }}"]) --}}

    @include('emails.partials.p', ['content' => "Thanks!"])

    @include('emails.partials.p', ['content' => "- Your friends at Trace"])

@endsection