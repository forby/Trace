@extends('emails.template')

@section('body')

    @include('emails.partials.h1', ['content' => 'Guess what?'])

    @include('emails.partials.p', ['content' => 'Your article has been published on Trace. We gave it a look and everything looks great. You should be proud, go check it out!'])

   @include('emails.partials.p-open')
   <br>
   @include('emails.partials.p-close')

    @include('emails.partials.button', ['content' => 'Go read it', 'url' => url('article/'.$article->slug) ])

    @include('emails.partials.p-open')
    <br>
    @include('emails.partials.p-close')

    @include('emails.partials.p', ['content' => "Thanks!"])

    @include('emails.partials.p', ['content' => "- Your friends at Trace"])

@endsection