@extends('emails.template')

@section('body')

    @include('emails.partials.h1', ['content' => 'Hey ' . $author->first_name . ','])

    @include('emails.partials.p', ['content' => "We've reviewed your article and we have some notes:"])

    @include('emails.partials.p', ['content' => " "])

    @include('emails.partials.p', ['content' => $body])

    @include('emails.partials.p-open')
    <br>
    @include('emails.partials.p-close')

    @include('emails.partials.button', ['content' => 'Go to the article', 'url' => url('article', $article->slug) ])

    @include('emails.partials.p-open')
    <br>
    @include('emails.partials.p-close')

    @include('emails.partials.p', ['content' => "Thanks!"])

    @include('emails.partials.p', ['content' => "- Your friends at Trace"])

@endsection