@extends('partials.userblock')

@section('user-block-header')
@endsection

@section('user-block-content')
    <p class="comment-intro">Comment</p>
    {!! Form::open(array('url' => 'api/comment/new', 'class' => 'comment-form')) !!}
        <div class="errors"></div>
        {!! Form::textarea('message', null, ['class' => 'form-control comment-message autosize', 'rows' => '1', 'placeholder' => 'Start typing...']) !!}
        {!! Form::hidden('article_id', $article->id) !!}
        {!! Form::submit('Post', ['class' => 'button hollow hidden']) !!}
    {!! Form::close() !!}
@endsection