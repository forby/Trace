@extends('global')

@section('globalContent')

	<header class="main-header">
		<div class="container">
			@include('partials.header')
		</div>
	</header>
	
	<div class="main-under-header">		

		{{-- Main Content --}}
		<main class="main-content">
			<div class="container">
				@include( 'flash::message' )
			</div>

			@yield('content')
		</main>

		{{-- Overlay --}}
		<div class="overlay"></div>

		<footer class="main-footer">
			@include('partials.footer')
		</footer>

	</div>

	{{-- Search Modal --}}
	@if ( Auth::check() )

		<div id="search-modal" class="search-modal is-hidden">
			{{-- Search Form --}}	
			<form class="search-form">
				<div class="container">
					<span class="intro">Search by typing</span>
					<input class="search-field" id="search-field" type="search" />
					<div class="help">
						<a href="{{ url('about') }}">Can't find what you're looking for?</a>
					</div>
				</div>
			</form>

			{{-- Close Button --}}
			<a class="close close-trigger" id="close-search" href="#"><span></span></a>
		</div>

		{{-- Navigation --}}
		@include('partials.navigation')

	@endif

	@yield('page-end')

@endsection
