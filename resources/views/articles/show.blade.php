@extends('app')

@section('content')

<div class="cover-photo" style="background-image: url('{{ $article->photo }}');">
	@if( $article->user == $user || $user->can('moderate-story') )
		<a href="{{ action('ArticlesController@edit', $article->slug) }}" class="button edit-article">Edit Article</a>
	@endif
	{{-- @if ( $article->superBumped )
		<div class="article-super container thin">
			<img src="{{ asset('img/emoji/bump-super.svg') }}">
		</div>
	@endif --}}
</div>

<article class="article-detail">
	<div class="container thin">

		<section class="body">
		 	@if ( ! $article->published )
		 		<div class="under-review">
		 			<p>Currently under review</p>
		 		</div>
		 	@endif
			<h1 class="heading">{{ $article->title }}</h1>

			<h2 class="subheading">Overview</h2>
			<div class="overview">
				{!! $article->overview !!}
			</div>

			<h2 class="subheading">Example</h2>
			<div class="example">
				{!! $article->example !!}
			</div>

			<h2 class="subheading">Advice</h2>
			<div class="advice">
				{!! $article->advice !!}
			</div>
		</section>

		<footer>

		@if ( $article->published )
			<section class="bump">
				<div class="message">Was this helpful?</div>

				<a href="#" class="trigger{{ $article->liked() ? ' bumped' : '' }}">
					<img src="{{ asset('img/emoji/bump.svg') }}">
				</a>

				<div class="count">
					<span class="you"><span class="active">You</span> and </span>
					<span class="them"><span class="amount">{{ $article->othersBumped }}</span> Bumped this.</span>
				</div>
			</section>
		@endif

			<section class="user">
				<div class="user-avatar">
					@include('partials.user-avatar-small', ['user' => $article->user])
				</div>
				<div class="user-meta">
					@include('partials.user-link', ['user' => $article->user])
					@if( $article->user->pro_title )
						<p class="professional-title">{{ $article->user->pro_title }}</p>
					@endif
					<p class="time">Written {{ $article->created_at->format('F j') }}</p>
				</div>
			</section>
			
			<section class="category">
				Filed under: <a href="{{ url('category', $article->category->slug) }}">{{ $article->category->name }}</a>
			</section>

			@unless ( $article->tags->isEmpty() )
				<section class="tags">
					<ul class="tags">
						<li class="title">Tags:</li>
						@foreach ( $article->tags as $tag )
							<li><a href="{{ url('tag', $tag->slug) }}">{{ $tag->name }}</a></li>
						@endforeach
					</ul>
				</section>
			@endunless

			{{-- START ADMIN APPROVAL UI --}}
			@if ( $user->can('moderate-story') && ! $article->published )
				<section class="moderate actions">
		            <ul>
	                    <li><a class="red" href="{{ action('ArticlesController@publish', $article->slug) }}">Approve</a></li></li>
	                    <li><a href="#" id="request-edits">Request Edits</a></li></li>
	                    <li><a href="#" class="cd-popup-trigger">Delete</a></li></li>
	                </ul>

	                @include('articles.request')

		        </section>
			@endif
			{{-- END ADMIN APPROVAL UI --}}

			{{-- <a href="{{ action('ArticlesController@edit', $article->slug) }}">Edit Article</a> --}}

		</footer>
	</div>
</article>

@if ( $article->published )

	<div class="container article-divider">
		<hr>
	</div>

	<section class="article-comments">
		<div class="container thin">

			{{-- Comment Form --}}
			@include('comments.commentform', ['class' => 'user-comment'])

			{{-- Comments Block --}}
			@include('partials.articles.comments')
		</div>
	</section>

	@if ( $relatedArticles->count() > 0 )
		<div class="container">
			<hr>
		</div>
		
		<section class="related-articles">
			<div class="container">
				<h2 class="page-title">Related Articles</h2>
				<div class="articles">
					@foreach( $relatedArticles as $related_article )
						@include('partials.articles.verticalMedium', ['article' => $related_article])
					@endforeach
				</div>
			</div>
		</section>
	@endif

@endif

@endsection

@section('page-end')
    @include('partials.modals.delete-article', $user)
@endsection

@section('page-js')
	<script type="text/javascript">
	/**
	 * Inline Ajax Comments
	 */
	$(function() {

		// Store Form
		var form = $('.comment-form');
		var btn = form.find('.button');

		form.find('.comment-message').on('input change',function(e) {
			if ( $(this).val().length > 0 ) {
				btn.removeClass('hidden');
			} else {
				btn.addClass('hidden');
			}
		});

		// Comment Submission
		form.submit(function(e) {
			// prevent the usual form submission
			e.preventDefault();

			// Clear errors
			form.find('.errors').html('');

			// Change the button text
			btn.val('Just a sec');

			// make ajax post
			$.post(
				form.prop('action'), 
				form.serialize(), 
				function(message) {					// SUCCESS
					// put comment into html
					if ( $('#comments').children().length > 0 ) {
						$('#comments > .comment').first().before(message);
					} else {
						$('#comments').append(message);
					}

					// Clear the form
					form.trigger('reset');

					// Reset height of textarea
					form.find('.comment-message').css('height', '45px');

					// Hide post button
					btn.addClass('hidden');
				}
			).fail(function(e) {					// FAILURE
				// get message content
				var message = JSON.parse(e.responseText).message[0];
				form.find('.errors').html(message);
			}).always(function() {
				// either way, clear the button focus
				form.find('.button').blur();
			});
		});

		/**
		 * Delete a Comment
		 */
		$('#comments').on('click', '.delete-comment', function(e) {
			e.preventDefault();
			
			var button = $(this);
			var comment = button.closest('.comment');
			var id = button.data('comment-id');
			var url = '/api/comment/'+id+'/delete';

			// Update the text while waiting
			button.html('Deleting...');

			// talk to server
			$.get(
				url, 
				function( data ) {
					// Success, remove the Comment from view
					comment.remove();					
				}
			).fail(function(e) {
				// Error, show message
				console.log(e.responseText);

				// Restore Text
				button.html('Delete');
			});
		});

		/**
		 * Like button actions
		 */
		$('.bump .trigger').click(function(e) {
			e.preventDefault();

			var btn = $(this),
				url;

			if ( btn.hasClass('bumped') ) {
				url = '/api/article/unbump/{{ $article->id }}';

				$.get(
					url, 
					function( data ) {
						// Alter button state
						btn.removeClass('bumped');

						// Un bumped, show sorrow
						btn.prev().removeClass('active').html('So not cool.');

						// Update the bump count
						// $('.bump .bump-count-string').html( data.human );
					}
				);
			} else {
				url = '/api/article/bump/{{ $article->id }}';

				$.get(
					url, 
					function( data ) {
						// Alter button state
						btn.addClass('bumped');

						// Thank them!
						btn.prev().addClass('active').html('Thanks, pal!');
						
						// Update the bump count
						// $('.bump .bump-count-string').html( data.human );
					}
				);
			}
		});

		/**
		 * Delete Article
		 */
		$('#delete-article').click(function(e) {
		    e.preventDefault();

		    $.post(
		        '{{ action( "ArticlesController@destroy", $article->slug ) }}', 
		        null, 
		        function( response ) {
		        	if (response.url) {
		        		window.location = response.url;
		        	}
		        }
		    );
		});

		/**
		 * Show Request Edits
		 */
		$('#request-edits').click(function(e) {
			e.preventDefault();

			$('.request-edits-form').addClass('show');
		});

		/**
		 * Send The Edits
		 */
		$('#submit-edits').click(function(e) {
			e.preventDefault();

			// get the form
			var form = $('.request-edits-form form');

			if ( form.find('textarea').val() == "" ) {
				form.find('.alert')
					.html('Whoops. Please add a message.')
					.removeClass('success')
					.addClass('show');

				return false;
			}

			// make ajax post
			$.post(
				form.prop('action'), 
				form.serialize(), 
				function(message) {					// SUCCESS
					form[0].reset();
					form.find('.alert')
						.html('The edits have been sent!')
						.addClass('show success');
				}
			).fail(function(e) {					// FAILURE
				form.find('.alert')
					.html('There was an error submitting the edits. Please try again.')
					.removeClass('success')
					.addClass('show');
			});
		});
	});
	</script>
@endsection
