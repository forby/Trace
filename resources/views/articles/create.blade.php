@extends('app')

@section('content')
	
{!! Form::model($article = new \App\Article, ['url' => 'articles', 'enctype' => 'multipart/form-data', 'class' => 'article-form create']) !!}
	{{-- Get Form template and pass in button text --}}
	@include ('articles.form', ['submitButtonText' => 'Submit'])
{!! Form::close() !!}

@endsection