<div class="request-edits-form">
    {!! Form::open(['action' => ['ArticlesController@sendRequest', $article->slug], 'method' => 'post', 'class' => 'article-form edit']) !!}

    <div class="alert"></div>

    <div class="form-group main-group">
        {!! Form::label('message', 'Message to the author') !!}

        {{-- Form::textarea needs name of field, default value, and added params --}}
        {!! Form::textarea('message', null, ['class' => 'form-control autosize', 'rows' => '1', 'placeholder' => 'Write your requests right here.']) !!}
    </div>

    <div class="form-group form-controls">
        {!! Form::submit('Send it!', ['class' => 'button hollow gray full form-control', 'id' => 'submit-edits']) !!}
    </div>

    {!! Form::close() !!}
</div>