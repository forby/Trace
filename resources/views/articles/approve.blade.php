@extends('app')

@section('content')
    <div class="container article-results waiting-for-approval">
        <section>
            
            <h1 class="page-title">Waiting for Approval</h1>

            @include('articles.list', ['listClass' => 'quad no-stars'])
            
        </section>
    </div>
@endsection