{{-- Feature Image --}}
<div class="cover-photo upload{{ $article->photo ? ' dropped' : '' }}" style="background-image:url('{{ $article->photo or '' }}');">
    <a class="button" href="#">Add Cover Photo</a>
    <div class="default">
    	<h2>Cover Photo</h2>
        <h4>We're all artists here. Make sure to share a pretty picture.</h4>
        <h4>We recommend 1500 x 500 px and under 4MB</h4>
    </div>
    {!! Form::file('photo', ['accept' => 'image/png,image/jpeg,image/jpg,image/gif']) !!}
    {!! Form::hidden('hasPhoto', $article->photo) !!}
</div>

<div class="container thin">

	@include ('errors.list')

	{{-- Title input --}}
	<div class="form-group">
		{!! Form::label('title', 'Title') !!}

		{{-- Form::text needs name of field, default value, and added params --}}
		{!! Form::textarea('title', null, ['class' => 'form-control autosize countWords', 'rows' => '1', 'placeholder' => 'Make it snappy! (10-words or less)']) !!}
	</div>

	{{-- Overview input --}}
	<div class="form-group">
		{!! Form::label('overview', 'Overview') !!}

		{{-- Form::textarea needs name of field, default value, and added params --}}
		{!! Form::textarea('overview', null, ['class' => 'form-control autosize editable countWords', 'rows' => '1', 'data-placeholder' => 'Kind of like a summary.']) !!}
	</div>

	{{-- Example input --}}
	<div class="form-group">
		{!! Form::label('example', 'Example') !!}

		{{-- Form::textarea needs name of field, default value, and added params --}}
		{!! Form::textarea('example', null, ['class' => 'form-control autosize editable countWords', 'rows' => '1', 'data-placeholder' => 'A real life example of your experience.']) !!}
	</div>

	{{-- Advice input --}}
	<div class="form-group">
		{!! Form::label('advice', 'Advice') !!}

		{{-- Form::textarea needs name of field, default value, and added params --}}
		{!! Form::textarea('advice', null, ['class' => 'form-control autosize editable countWords', 'rows' => '1', 'data-placeholder' => 'What would you tell yourself if you could go back in time?']) !!}
	</div>

	<div class="word-count">
		<span class="message"></span><span class="count"></span>
	</div>

	{{-- Category input --}}
	<div class="form-group pretty-select-wrap">
		{!! Form::label('category_id', 'Category') !!}
		<span>{{ $article->category->name or head( $categories ) }}</span>
		{!! Form::select('category_id', $categories, null, ['id'=>'category_id', 'class' => 'form-control pretty-select']) !!}
	</div>

	{{-- Tags input --}}
	<div class="form-group">
		{!! Form::label('tag_list', 'Tags') !!}

		{{-- Create date field with a default value of now --}}
		{{-- tags[] designates an array input will be passed on submit --}}
		{!! Form::select('tag_list[]', $tags, null, ['placeholder' => 'Add up to three tags…', 'id' => 'tag_list', 'class' => 'form-control', 'multiple']) !!}
	</div>

	@if ( $user->can('post-as-users') && isset($users) )
	<div class="form-group">
		{!! Form::label('post_as_user', 'Post as') !!}

		<p class="note">If left blank, this article will post from your account.</p>

		{{-- Create date field with a default value of now --}}
		{{-- tags[] designates an array input will be passed on submit --}}
		{!! Form::select('post_as', $users, null, ['placeholder' => 'Choose an author…', 'id'=>'user_list', 'class' => 'form-control']) !!}
	</div>
	@endif

	{{-- Submit button --}}
	<div class="form-group form-controls">
		{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}

		@if ( Request::is('*/edit') && $user->can('remove-articles') )
			<a href="#" class="cd-popup-trigger button hollow fixed delete-post">Delete</a>
		@endif
	</div>
</div>

@section('page-end')
    @include('partials.modals.delete-article', $user)
@endsection

@section('page-js')
	<script type="text/javascript">

		$('#tag_list').selectize({
			create: true,
			maxItems: 3,
			openOnFocus: true,
			maxOptions: 3
		});

		$('#user_list').selectize();

		// Medium Editors
		var editor = new MediumEditor('.editable', {
			buttonLabels: 'fontawesome',
			toolbar: {
				buttons: ['bold', 'italic', 'anchor']
			},
			anchor: {
		        linkValidation: true
		    }
		});

		// Dropdown Select
		$('.pretty-select').hide().parent().click(function(e) {
			e.preventDefault();
			
			var select = $(this).find('select');
			var label = $(this).find('label');
			var div = $('<div/>', {
				class: 'pretty-select-dropdown'
			});

			select.children().each(function() {
				
				var selected = ( $(this).prop('selected') == true ) ? 'active' : '';

				var link = $('<a/>', {
					href: '#',
					class: selected,
					html: $(this).html()
				}).click(function(e) {
					e.preventDefault();
					e.stopPropagation();

					// Make this one active
					$(this).addClass('active').siblings().removeClass('active');

					// Update the label
					select.siblings('span').html($(this).html());

					// change the form object
					select.children().eq( $(this).index() ).prop('selected', true);

					// Close the box
					div.remove();
				});

				div.append( link );
			});

			// Add it
			$(this).append( div );

			// place it
			div.css({
				left: label.outerWidth() - 7,
				top: (label.outerHeight() / 2) - (div.outerHeight() / 2)
			});

			// close action
			$(document).on('mouseup touchend', function(e) {
				if (!(div.is(e.target) || $.contains(div[0], e.target)) && div.hasClass('is-visible')) {
					div.remove();
				}
			});

			// show it
			div.addClass('is-visible');
		});
		
		// get the photo holder
		var holder = $('.cover-photo');

		// Button to trigger the file upload dialogue
		holder.find('a').click(function(e) {
			e.preventDefault();
			$(this).siblings('input').trigger('click');
		});

		// Drop states
		holder.on('dragover', function() {
			holder.addClass('hover');
		}).on('dragleave', function() {
			holder.removeClass('hover');
		});

		var file_added_text = 'REPLACE COVER PHOTO';
		if( holder.css('background-image') != 'none' ) {
			holder.find('a').html(file_added_text);
		}

		// Handle Dropping image
		holder.find('input').on('change', function(e) {
		    var file = e.target.files[0];

		    // update button text
		    holder.find('a').html(file_added_text).blur();
		    
		    // States if needed
		    holder.removeClass('hover');
		    holder.addClass('dropped');
		    
		    // ABORT, not an image
		    // Consider a new way of alerting
		    if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
		      return alert('File type not allowed.');
		    }
		    
		    // load the image
		    var reader = new FileReader(file);
		    reader.readAsDataURL(file);

		    reader.onload = function(e) {
		      var data = e.target.result

		      // Add it in
		      holder.attr('style', 'background-image:url("'+data+'");');
		    };
		});

		// Word countdown
		var areas = $('.countWords:not(.medium-editor-hidden)'),
			limit = 500;
			count_wrap = $('.word-count');
			count_el = $('.word-count .count');
			message_el = $('.word-count .message');

		// Word Count Helper
		function wordCount( val ){
		    return val.match(/\S+/g).length;
		}

		function calculateWords() {
			var val = 0;

			areas.each(function() {
				var text;
				if ( this.nodeName === "DIV" ) {
					text = $(this).text();
				} else {
					text = this.value;
				}

				var words = text.match(/\S+/g);

				if ( words ) {
					val += parseInt(words.length);
				}
			});

			if ( val > 500 ) {
				message_el.html('A little long winded, eh?');
			} else if ( val > 400 ) {
				message_el.html('OK, wrap it up...');
			} else if ( val > 300 ) {
				message_el.html('Looking good!');
			} else if ( val > 0 ) {
				message_el.html('Keep it short and sweet.');
			} else {
				message_el.html('');
			}

			val = limit - val;

			count_el.html( val );
			count_wrap.toggleClass('error', ( val < 0 ));
		}

		areas.on('input', calculateWords);
		calculateWords();

		/**
		 * Delete Article
		 */
		$('#delete-article').click(function(e) {
		    e.preventDefault();

		    $.post(
		        '{{ action( "ArticlesController@destroy", $article->slug ) }}', 
		        null, 
		        function( response ) {
		        	if (response.url) {
		        		window.location = '{{ url("articles") }}';
		        	}
		        }
		    );
		});

	</script>
@endsection