<div class="not-sidebar">
    <div class="article-list {{ $listClass or 'double' }}">
        @forelse( $articles as $article )
            @include('partials.articles.verticalMedium', ['article' => $article])
        @empty
            <p>We didn't find any articles :(</p>
        @endforelse
    </div>

    @if ( is_a( $articles, 'Illuminate\Pagination\LengthAwarePaginator' ) )
        @include('partials.articles.pagination')
    @endif
</div>