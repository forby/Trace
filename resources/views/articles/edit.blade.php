@extends('app')

@section('content')
	
{!! Form::model($article, ['method' => 'PATCH', 'action' => ['ArticlesController@update', $article->id], 'class' => 'article-form edit', 'files' => true]) !!}
	{{-- Get Form template and pass in button text --}}
	@include ('articles.form', ['submitButtonText' => 'Save it!'])
{!! Form::close() !!}

@endsection