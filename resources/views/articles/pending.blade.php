@extends('app')

@section('content')
    <div class="container thin head-space foot-space pending">
        <div class="peace-out">
            <img src="{{ asset('img/emoji/peace.svg') }}">
        </div>

        <p class="text-center huge serif">
            Thanks for submitting your story! We’ll proofread it before publishing or let you know if we have any notes. Peace out.
        </p>

        <div class="back-home text-center">
            <a href="{{ url('/') }}">Back To Home</a>
        </div>
    </div>
@endsection