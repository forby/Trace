@extends('app')

@section('content')
    <div class="container article-results">
        <section class="not-sidebar">
            
            <h1 class="page-title">Top Stories</h1>

            @include('articles.list', ['listClass' => 'third'])
            
        </section>
    </div>
@endsection