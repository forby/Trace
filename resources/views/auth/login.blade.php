@extends('gateway')

@section('content')
	
	<div class="site-gateway">
		<div class="site-logo">
			<img src="{{ asset('img/trace.svg') }}">
		</div>
		<div class="site-auth">
			{{-- <div id="site-login" class="site-login{{ old('register') ? ' is-hidden' : '' }}">
				@include('auth.forms.login')
				<p class="site-auth-switch">Or <a href="#" class="vis-toggle" data-hide="#site-login" data-show="#site-register">Create an Account</a></p>
			</div>
			<div id="site-register" class="site-register{{ old('register') ? '' : ' is-hidden' }}">
				@include('auth.forms.register')
				<p class="site-auth-switch">Or <a href="#" class="vis-toggle" data-hide="#site-register" data-show="#site-login">Sign in</a></p>
			</div> --}}
			<div id="site-cas">
				<a href="{{ action('Auth\AuthController@getCasLogin') }}" class="button large full text-center">Sign In with MySCAD</a>
			</div>
		</div>
	</div>
	
@endsection