<form class="sign-in" role="form" method="POST" action="{{ url('/login') }}">
    <input type="hidden" name="remember" value="Yes">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    @include ('errors.list')

    <div class="form-group {{ $errors->has('email') ? 'error' : '' }}">
        {{-- <label for="email">E-Mail Address</label> --}}
        <input type="email" name="email" value="{{ old('email') }}" placeholder="SCAD Email">
    </div>

    <div class="form-group password {{ $errors->has('password') ? 'error' : '' }}">
        {{-- <label for="password">Password</label> --}}
        <span class="forgot"><a href="{{ url('forgot') }}" tabindex="-1" class="forgot-password">?</a></span>
        <input type="password" name="password" placeholder="Password">
    </div>

    <div class="form-group">
        <input type="submit" value="Sign In"> 
        {{-- <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a> --}}
    </div>
    
</form>