<form role="form" method="POST" class="create-account" action="{{ url('/register') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    @include ('errors.list')

    <div class="form-group {{ $errors->has('name') ? 'error' : '' }}">
        {{-- <label for="name">Name</label> --}}
        <input type="text" name="name" value="{{ old('name') }}" placeholder="Name">
    </div>

    <div class="form-group {{ $errors->has('email') ? 'error' : '' }}">
        {{-- <label for="email">E-Mail Address</label> --}}
        <input type="email" name="email" value="{{ old('email') }}" placeholder="SCAD Email">
    </div>

    <div class="form-group {{ $errors->has('password') ? 'error' : '' }}">
        {{-- <label for="password">Password</label> --}}
        <input type="password" name="password" placeholder="Password">
    </div>

    <div class="form-group {{ $errors->has('password_confirmation') ? 'error' : '' }}">
        {{-- <label for="password_confirmation">Confirm Password</label> --}}
        <input type="password" name="password_confirmation" placeholder="Confirm Password">
    </div>

    <div class="form-group">
        <input type="submit" value="Create Account">
    </div>
    
</form>