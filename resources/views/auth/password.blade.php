@extends('app')

@section('content')
<div class="container thin head-space foot-space">

	<h1 class="page-title">Let's reset that password.</h1>

	@if (session('status'))
		<div class="alert success">
			{{ session('status') }}
		</div>
	@endif

	@include('errors.list')

	<form class="article-form" role="form" method="POST" action="{{ url('/forgot') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="form-group">
			<div class="form-group">
				<input type="email" class="form-control clean" name="email" value="{{ old('email') }}" placeholder="Email address">
			</div>
		</div>

		<div class="form-group form-controls">
			<button type="submit" class="button hollow gray full large">
				Send me the link!
			</button>
		</div>
	</form>
</div>
@endsection
