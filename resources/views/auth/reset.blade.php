@extends('app')

@section('content')
<div class="container thin head-space">

	<h1 class="page-title">Reset Password</h1>

	@include('errors.list')

	<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="token" value="{{ $token }}">

		<div class="form-group">
			<input type="email" class="form-control clean" name="email" value="{{ old('email') }}" placeholder="Email address">
		</div>

		<div class="form-group">
			<input type="password" class="form-control clean" name="password" placeholder="New password">
		</div>

		<div class="form-group">
			<input type="password" class="form-control clean" name="password_confirmation" placeholder="New password again">
		</div>

		<div class="form-group form-controls head-space">
			<button type="submit" class="button hollow full gray large">
					Reset Password
			</button>
		</div>
	</form>

</div>
@endsection
