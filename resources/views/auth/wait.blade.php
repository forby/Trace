@extends('app')

@section('content')

    <div class="container thin head-space foot-space">
        <h1 class="page-title">So close, yet <em>so close!</em></h1>
        <p class="large">We're almost there, {{ $name or 'friend' }}. Please check your email for a confirmation from us to complete your registration. We'll see you soon!</p>
    </div>
    
@endsection
