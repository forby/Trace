@extends('app')

@section('content')
    <div class="container article-results">
        <section class="not-sidebar">
            
            <h1 class="page-title">{{ $title or 'Featured Articles' }}</h1>

            @include('articles.list')
            
        </section>
    </div>
@endsection