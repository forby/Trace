@extends('app')

@section('content')

    {!! Form::model($feature, ['method' => 'PATCH', 'class' => 'article-form', 'action' => ['FeatureController@update'], 'files' => true]) !!}
        {{-- Get Form template and pass in button text --}}
        @if ( $feature )
            <div class="cover-photo upload dropped"  style="background-image: url('{{ $feature->photo }}');">
        @else
            <div class="cover-photo upload">
        @endif
            <a class="button" href="#">Add Cover Photo</a>
            <div class="default">
                <h2>Cover Photo</h2>
                <h4>We're all artists here. Make sure to share a pretty picture.</h4>
                <h4>We recommend 1500 x 500 px and under 4MB</h4>
            </div>
            {!! Form::file('image', ['accept' => 'image/png,image/jpeg,image/jpg,image/gif']) !!}
            @if( $feature )
                {!! Form::hidden('hasPhoto', $feature->photo) !!}
            @endif
        </div>

        <div class="container thin">

            @include ('errors.list')

            <div class="form-group">
                {!! Form::label('title', 'Title') !!}

                {{-- Form::textarea needs name of field, default value, and added params --}}
                {!! Form::textarea('title', null, ['class' => 'form-control autosize clean', 'rows' => '1', 'placeholder' => 'Something the world will see!']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Description') !!}

                {{-- Form::textarea needs name of field, default value, and added params --}}
                {!! Form::textarea('description', null, ['class' => 'form-control autosize clean', 'rows' => '1', 'placeholder' => 'Concise but powerful. We want people clicking!']) !!}
            </div>

            <div class="form-group">
                <div class="gutter-block">
                    <div class="gutter">
                        {!! Form::checkbox('reset', null) !!}
                    </div>
                    <header>{!! Form::label('reset', 'Reset Articles') !!}</header>
                    <main>
                        <p class="form-description">All previous articles will be reset.</p>
                    </main>
                </div>
            </div>

            <div class="form-group">
                <div class="gutter-block">
                    <div class="gutter">
                        <div class="star disabled outline gutter-item"><img src="{{ asset('img/aspect-square.gif') }}"></div>
                    </div>
                    <header>{!! Form::label('star', 'Feature') !!}</header>
                    <main>
                        <p class="form-description">Use the star to feature the next set of articles. Selections will automatically fall into the featured section.</p>
                    </main>
                </div>
            </div>

            <div class="form-group form-controls">
                <ul>
                    <li>{!! Form::submit('Save', ['class' => 'button hollow fixed foot-space']) !!}</li>
                    <li><a href="#" id="reset-feature" class="button hollow gray fixed">Clear Feature</a></li>
                </ul>
            </div>

        </div>
    {!! Form::close() !!}

    {!! Form::open( ['action' => 'FeatureController@destroy', 'id' => 'reset-feature-form', 'method' => 'delete'] ) !!}
    {!! Form::close() !!}

@endsection

@section('page-js')
    <script type="text/javascript">
    $(function() {

        $('#reset-feature').click(function(e) {
            e.preventDefault();

            $('#reset-feature-form').submit();
        });

        // get the photo holder
        var holder = $('.cover-photo');

        // Button to trigger the file upload dialogue
        holder.find('a').click(function(e) {
            e.preventDefault();
            $(this).siblings('input').trigger('click');
        });

        // Drop states
        holder.on('dragover', function() {
            holder.addClass('hover');
        }).on('dragleave', function() {
            holder.removeClass('hover');
        });

        var file_added_text = 'REPLACE COVER PHOTO';
        if( holder.css('background-image') != 'none' ) {
            holder.find('a').html(file_added_text);
        }

        // Handle Dropping image
        holder.find('input').on('change', function(e) {
            var file = e.target.files[0];

            // update button text
            holder.find('a').html(file_added_text).blur();
            
            // States if needed
            holder.removeClass('hover');
            holder.addClass('dropped');
            
            // ABORT, not an image
            // Consider a new way of alerting
            if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
              return alert('File type not allowed.');
            }
            
            // load the image
            var reader = new FileReader(file);
            reader.readAsDataURL(file);

            reader.onload = function(e) {
              var data = e.target.result

              // Add it in
              holder.attr('style', 'background-image:url("'+data+'");');
            };
        });
        
    });
    </script>
@endsection