@extends('app')

@section('content')
    <div class="container article-results has-sidebar">
        <section class="not-sidebar">
            
            <h1 class="page-title">Tagged with <span class="stand-out">{{ $tag->name }}</span></h1>

            @include('articles.list')
            
        </section>

        <aside class="sidebar">
            @include('sidebar.popular_tags', ['tags' => $top_tags])
        </aside>
    </div>
@endsection