@extends('app')

@section('content')
    <div class="container thin head-space foot-space">
        
        <h1>About</h1>

        <p>Trace is an educational resource for SCAD students, professors and alumni to share insights and experiences for how to work together effectively and efficiently in teams. Trace is brought to you by the Collaborative Learning Center, a branch of the Savannah College of Art and Design, dedicated to teamwork, professional projects and encouraging high-fives.</p>

        <p>Read, write, bump and comment on articles to interact with a growing network of experienced collaborators.</p>

        <h3>Bumps</h3>

        <p>At the bottom of each article is a Fist Bump. A Bump is our way of showing affirmation and endorsing quality content. If you found a story helpful, bump it! The top 20 stories on the site receive a coveted Super Bump.</p>

        <p class="text-center"><img src="{{ asset('img/emoji/bump-super.svg') }}" width="200"></p>

        <h3>Help</h3>
        
        <p>Don’t see a story or subject you are looking for? You could try writing about it! Or you can suggest a topic for us. If you have suggestions, you’re probably not the only one. Fill out your information below with your recommendations. Anything we can do to make Trace better is invaluable!</p>

        {!! Form::open( array('url' => 'contact/send', 'method' => 'post', 'class' => 'head-space', 'id' => 'contact-form') ) !!}

        @if( Session::has('sent') )
            <div class="alert success">
                {{ Session::get('sent') }}
            </div>
        @endif

        @include('errors.list')

        @if ( Auth::check() )
            <input type="hidden" name="email" value="{{ Auth::user()->email }}">
            <input type="hidden" name="name" value="{{ Auth::user()->name }}">
        @else
            <div class="form-group {{ $errors->has('name') ? 'error' : '' }}">
                <input type="text" class="clean form-control" name="name" value="{{ old('name') }}" placeholder="Name">
            </div>

            <div class="form-group {{ $errors->has('email') ? 'error' : '' }}">
                <input type="email" class="clean form-control" name="email" value="{{ old('email') }}" placeholder="Email address">
            </div>
        @endif

        <div class="form-group {{ $errors->has('subject') ? 'error' : '' }}">
            <input type="text" class="clean form-control" name="subject" value="{{ old('subject') }}" placeholder="Subject">
        </div>

        <div class="form-group {{ $errors->has('message') ? 'error' : '' }}">
            {!! Form::textarea('message', null, ['class' => 'form-control autosize clean', 'rows' => '1', 'placeholder' => 'Questions or comments']) !!}
        </div>

        <div class="form-group form-controls">
            {!! Form::submit('Send it now', ['class' => 'button full large form-control']) !!}
        </div>

        {!! Form::close() !!}

    </div>
@endsection