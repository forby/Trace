<section class="top-tags">
    <header>
        <h2 class="sidebar-heading">Popular Tags</h2>
    </header>

    <ul class="tags">
        @forelse( $tags as $tag )
            <li><a href="{{ url('tag', $tag->slug) }}">{{ $tag->name }}</a></li>
        @empty
            <p>No tags :(</p>
        @endforelse
    </ul>

</section>