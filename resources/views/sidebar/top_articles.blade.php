<section class="top-articles">
    <header>
        <h2 class="sidebar-heading">Top Stories</h2>
        {{-- <a href="#" class="sub-right">View all &rarr;</a> --}}
    </header>

    <ul>
        @forelse( $articles as $article )
            <li>
                <a class="title" href="{{ url('article', $article->slug) }}">{{ $article->title }}</a>
                <a class="name" href="{{ url('user', $article->user->slug) }}">{{ $article->user->name }}</a>
            </li>
        @empty
            <p>No top stories :(</p>
        @endforelse
    </ul>

    @if ( $articles->count() > 0 )
        <div class="more">
            <a href="{{ url('top-stories') }}">View all Top Stories &rarr;</a>
        </div>
    @endif

</section>