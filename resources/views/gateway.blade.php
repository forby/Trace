@extends('global')

@section('globalContent')

    <div class="main-under-header site-gateway-wrap">     

        {{-- Main Content --}}
        <main class="main-content">
            <div class="container">
                @include( 'flash::message' )
            </div>

            @yield('content')
        </main>

        <footer class="main-footer">
            @include('partials.footer')
        </footer>

    </div>

@endsection