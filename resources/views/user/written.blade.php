@extends('app')

@section('content')
    <div class="container article-results">
        <section class="not-sidebar">
            
            <h1 class="page-title">Articles from <span class="stand-out">{{ $user->name }}</span></h1>

            @include('articles.list', ['listClass' => 'third'])
            
        </section>
    </div>
@endsection