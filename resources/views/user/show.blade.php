@extends('app')

@section('content')
    <div class="container user-profile head-space">
        
        @if (Auth::user()->id == $user->id || Auth::user()->can('manage-users'))
            <section class="actions">
                <ul>
                    <li><a href="{{ action('UserController@edit', $user->slug) }}" class="button hollow gray">Edit Profile</a></li>
                </ul>
            </section>
        @endif

        <section class="profile-header">

            <div class="avatar-wrap">
                <div class="photo">
                    <div class="avatar frame{{ $user->avatar ? ' photo' : '' }}" style="background-image:url('{{ $user->avatar_image }}');"></div>
                </div>
                @if ( $user->superCount > 0 )
                    <div class="super-count">
                        <span>{{ $user->superCount }}</span>
                    </div>
                @endif
            </div>

            <div class="info">
                <div class="info-group">
                    <h1 class="name">{{ $user->name }}</h1>
                    @if ( $user->pro_title )
                        <h3 class="title">{{ $user->pro_title }}</h3>
                    @endif
                </div>

                <ul>
                    <li>
                        <span class="icon-location"></span>
                        <span>{{ $user->hometown != '' ? $user->hometown : 'Trace, USA' }}</span>
                    </li>
                    <li>
                        <span class="icon-education"></span>
                        <span>{{ $user->major != '' ? $user->major : 'Graduating Sometime!' }}</span>
                    </li>
                    <li>
                        <span class="icon-prize"></span>
                        <span>{{ $user->experience != '' ? $user->experience : 'CLC Experience?' }}</span>
                    </li>
                </ul>
            </div>
        </section>

        <section class="social">
            <ul>
                @if ( $user->linkedin )
                    <li><a href="http://linkedin.com/in/{{ $user->linkedin }}" target="_blank"><span class="icon-linkedin"></span></a></li>
                @endif
                @if ( $user->instagram )
                    <li><a href="http://instagram.com/{{ $user->instagram }}" target="_blank"><span class="icon-instagram"></span></a></li>
                @endif
                @if ( $user->twitter )
                    <li><a href="http://twitter.com/{{ $user->twitter }}" target="_blank"><span class="icon-twitter"></span></a></li>
                @endif
                @if ( $user->facebook )
                    <li><a href="http://facebook.com/{{ $user->facebook }}" target="_blank"><span class="icon-facebook"></span></a></li>
                @endif
                @if ( $user->public_email )
                    <li><a href="mailto:{{ $user->public_email }}" target="_blank"><span class="icon-email"></span></a></li>
                @endif
                @if ( $user->website )
                    <li><a href="http://{{ $user->website }}" target="_blank"><span class="icon-web"></span></a></li>
                @endif
            </ul>
        </section>

        <hr>

        {{-- @if ( Auth::user()->can('manage-users') && ! $user->hasRole('admin') )
            <p class="text-center"><a href="#" class="red cd-popup-trigger">Delete this user</a></p>
            <hr>
        @endif --}}

        <section class="stats">

            @include('user.stat', [
                'count' => $articles_written_count,
                'countString' => 'Published',
                'items' => $articles_written,
                'slug' => 'written'
            ])

            @include('user.stat', [
                'count' => $articles_bumped_count,
                'countString' => 'Bumped',
                'items' => $articles_bumped,
                'slug' => 'bumped'
            ])
            
            @include('user.stat', [
                'count' => $articles_commented_count,
                'countString' => 'Comments',
                'items' => $articles_commented,
                'slug' => 'commented'
            ])
        </section>
    </div>
@endsection

@section('page-end')
    @include('partials.modals.delete-user', $user)
@endsection

@section('page-js')
    <script type="text/javascript">
        $(function() {
            $('#delete-user').click(function(e) {
                e.preventDefault();

                $.post(
                    '/api/user/{{ $user->id }}/remove', 
                    null, 
                    function( response ) {
                        // They're gone, go home
                        window.location = '{{ url("/") }}';
                    }
                );
            });
        });
    </script>
@endsection