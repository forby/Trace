@extends('app')

@section('content')
    <div class="container thin user-management head-space">
        <h1>User Management</h1>

        <section>
            <h4>Administrators</h4>
            <p class="info top-flush">Users who can manage users and posts</p>
            <div class="user-group">
            @foreach ($admins as $admin)
                <div class="user">
                    <div class="name">{{ $admin->name }}</div>
                </div>
            @endforeach
            </div>

            <h4>Editors</h4>
            <p class="info top-flush">Users who can write posts</p>
            <div class="user-group">
            @foreach ($editors as $editor)
                <div class="user">
                    <div class="name">{{ $editor->name }}</div>
                    <div class="action"><a href="#" class="remove-editor" data-id="{{ $editor->id }}">Revoke Priveleges</a></div>
                </div>
            @endforeach
            {!! Form::open(['action' => ['UserController@makeUser'], 'method' => 'post', 'id' => 'make-user']) !!}
            {!! Form::hidden('id', null, ['id' => 'make-user-id']) !!}
            {!! Form::close() !!}
            </div>

            <h4>Add Editor</h4>
            <p class="info top-flush">Grand editor priveleges to a user</p>
            <div class="form-group">
                {!! Form::open(['action' => ['UserController@makeEditor'], 'method' => 'post']) !!}
                {!! Form::select('id', $users, null, ['placeholder' => 'Choose a user…', 'id'=>'user_list', 'class' => 'form-control']) !!}
                {!! Form::submit('Make Editor') !!}
                {!! Form::close() !!}
            </div>
        </section>

        <hr class="huge">

        <section>
            <h1>Custom Redirects</h1>
            <p class="info top-flush">Here you can add links to other sites and resources.</p>

            <div class="new-redirect">
                <h4>Add a new redirect:</h4>
                <div class="redirect add-form">
                @include('errors.list')
                {!! Form::open(['action' => ['UserController@createRedirect'], 'method' => 'post']) !!}
                    <div class="name">{!! Form::text('name', null, ['placeholder' => 'Name']) !!}</div>
                    <div class="url">{!! Form::text('url', null, ['placeholder' => 'http://wwww.example.com']) !!}</div>
                    <div class="action form">{!! Form::submit('Save') !!}</div>
                {!! Form::close() !!}
                </div>
            </div>

            <hr>

            <div class="redirects">
                @forelse ($redirects as $redirect)
                    <div class="redirect">
                    {!! Form::open() !!}
                        {!! Form::hidden('id', $redirect->id) !!}
                        <div class="name">/ <a href="{{ url($redirect->name) }}" target="_blank">{{ $redirect->name }}</a></div>
                        <div class="url">{!! Form::text('url', $redirect->url) !!}</div>
                        <div class="action"><a href="#" data-action="{{ action('UserController@updateRedirect') }}" id="update-redirect">Update</a> | <a href="#" data-action="{{ action('UserController@removeRedirect') }}" id="remove-redirect">Remove</a></div>
                    {!! Form::close() !!}
                    </div>
                @empty
                    <p>No Redirects</p>
                @endforelse
            </div>
            
        </section>

        <hr class="huge">

        <section>
            <h1>Stats</h1>
            <h5>Total Bumps: <span class="primary">{{ $totalBumps }}</span></h5>
            <h5>Total Comments: <span class="primary">{{ $totalComments }}</span></h5>
        </section>

    </div>
@stop

@section('page-js')
    <script type="text/javascript">
    $(function() {

        $('#user_list').selectize();

        $('.remove-editor').click(function(e) {
            e.preventDefault();

            // Get the id
            var id = $(e.target).data('id');

            // Update the value with the id
            $('#make-user-id').val(id);

            // Submit the form
            $('#make-user').submit();
        });

        $('.redirect .action a').click(function (e) {
            e.preventDefault();

            // info
            var item = $(e.target);
            var form = item.closest('form');

            // Update the form's action
            form.attr('action', item.data('action'));

            // Post the form
            form.submit();

        });

    });
    </script>
@endsection