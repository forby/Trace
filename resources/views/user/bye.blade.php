@extends('app')

@section('content')
    <div class="container thin user-settings">
        <h1 class="user-title"><span class="user">{{ $user->name }}: Settings:</span> Delete Account</h1>

        <section class="settings">            

            <div class="settings-block">
                <header>Delete Account</header>
                <main>
                    <p>You could have mistakenly arrived at this page. Or not.</p>
                    <p></p>
                    <p>We’ll be sad to see you go.</p>
                </main>
                <footer>
                    <ul class="wide">
                        <li><a href="#" id="confirm">Confirm</a></li>
                        <li><a href="{{ url('settings') }}" class="gray">Back</a></li>
                    </ul>
                </footer>
            </div>

            <div class="settings-block wave">
                <img src="{{ asset('img/emoji/wave.svg') }}">
            </div>
        </section>
    </div>
@stop

@section('page-js')
    <script type="text/javascript">
    $(function() {

        $('#confirm').click(function(e) {
            e.preventDefault();

            $.post(
                '/api/user/{{ $user->id }}/remove', 
                null, 
                function( response ) {
                    // They're gone, go home
                    window.location = '{{ url("/logout") }}';
                }
            );
        });

    });
    </script>
@endsection