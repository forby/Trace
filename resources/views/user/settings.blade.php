@extends('app')

@section('content')
    <div class="container thin user-settings">
        <h1 class="user-title"><span class="user">{{ $user->name }}:</span> Settings</h1>

        <section class="settings">
            
            <div class="settings-block gutter-block">
                <form id="notify-form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    
                    <header>Email Notifications</header>
                    <div class="gutter">
                        <input class="header-checkbox" id="notification-status" name="email_notify" type="checkbox"{{ $user->email_notify ? ' checked="checked"' : '' }}>
                    </div>
                    <main>
                        Notify me when someone comments on my article or finds my article helpful.
                    </main>
                    <footer>
                        <ul>
                            <li><a href="#" id="notification-trigger">Save</a></li>
                        </ul>
                    </footer>
                </form>
            </div>
            

            {{-- <div class="settings-block">
                <header>Password Reset</header>
                <main>
                    We’ll send you an email with steps to reset your password.
                    <div class="response">You've got mail! Check your inbox for instructions.</div>
                </main>
                <footer>
                    <ul>
                        <li><a href="#" id="reset-password">Yes, please!</a></li>
                    </ul>
                </footer>
            </div> --}}

            {{-- <div class="settings-block">
                <header>Delete Account</header>
                <main>Permanently delete account. This cannot be undone.</main>
                <footer>
                    <ul>
                        <li><a href="{{ action('UserController@delete', $user->slug) }}">Sadly, yes.</a></li>
                    </ul>
                </footer>
            </div> --}}
        </section>

        <form id="reset-form" role="form" method="POST" action="{{ url('/forgot') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="email" value="{{ $user->email }}">
        </form>

    </div>
@stop

@section('page-js')
    <script type="text/javascript">
    $(function() {

        $('#reset-password').click(function(e) {
            e.preventDefault();

            $.post(
                $('#reset-form').prop('action'),
                $('#reset-form').serialize(),
                function( data ) {
                    $('.response').addClass('show');

                    setTimeout(function() {
                        $('.response').removeClass('show');
                    },5000);
                }
            );
        });

        $('#notification-trigger').click(function(e) {
            e.preventDefault();

            var btn = $(this);

            // change the text
            btn.html('Saving...');

            $.post(
                '/api/user/{{ $user->id }}/update-notify',
                $('#notify-form').serialize(),
                function( data ) {
                    btn.html('Saved!');

                    setTimeout(function() {
                        btn.html('Save');
                    },1500);
                }
            );
        });

    });
    </script>
@endsection