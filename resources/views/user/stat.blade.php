<div class="stat">
    <div class="stat-number">
        <h2>{{ $count }}</h2>
        <span>{{ $countString }}</span>
    </div>

    <div class="articles">
        @forelse( $items as $item )
            @include('partials.articles.verticalMedium', ['article' => $item])
        @empty
            <p class="empty">Nothing to see here :(</p>
        @endforelse
    </div>

    @if ( $count > 3 )
        <div class="more">
            <a href="{{ url('user', [ $user->slug, $slug ]) }}">More</a>
        </div>
    @endif
</div>