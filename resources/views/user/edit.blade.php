@extends('app')

@section('content')
    <div class="container user-profile head-space editing">

        <section class="actions">
            <ul>
                <li><a href="#" class="button hollow gray" id="user-save">Save</a></li>
                <li><a href="{{ url('user', $user->slug) }}" class="button hollow gray">Cancel</a></li>
            </ul>
        </section>

        @include('errors.list')

        {!! Form::model($user, ['action' => ['UserController@edit', $user->slug], 'method' => 'post', 'id' => 'user-form', 'files' => true] ) !!}

        <section class="profile-header">
            <div class="avatar-wrap">
                <div class="photo">
                    @if ( $user->avatar )
                    <div class="avatar frame dropped" style="background-image:url('{{ $user->avatar }}');">
                    @else
                    <div class="avatar frame default" style="background-image:url('');">
                    @endif
                        <img class="default" src="{{ $user->avatar_default }}">
                        {!! Form::file('photo', ['accept' => 'image/png,image/jpeg,image/jpg,image/gif']) !!}
                        {!! Form::input('hidden', 'remove_avatar', null, ['id' => 'remove-avatar']) !!}
                    </div>
                </div>
                <div class="photo-edit">
                    <ul>
                        <li><a href="#" id="change-avatar">Change Your Face</a></li>
                        @if ( $user->avatar )
                            <li><a href="#" id="delete-avatar">Remove</a></li>
                        @else
                            <li><a href="#" id="delete-avatar" style="display:none;">Remove</a></li>
                        @endif
                    </ul>
                </div>
            </div>

            <div class="info no-pad">
                    <div class="info-group">
                        {{-- <h1 class="name">{{ $user->name }}</h1> --}}
                        {!! Form::input('text', 'name', null, array('class' => 'name form-control')) !!}
                        {!! Form::input('text', 'pro_title', null, array('placeholder' => 'Professional Title', 'class' => 'pro_title form-control')) !!}
                        {!! Form::input('hidden', 'user_id', $user->id) !!}
                    </div>

                    <div class="form-group info-group">
                        {{-- <label for="hometown">Hometown</label> --}}
                        {!! Form::label('hometown', 'Hometown') !!}
                        {!! Form::input('text', 'hometown', null, array('placeholder' => 'Where are you from? (eg, Houston, TX)', 'class' => 'form-control')) !!}
                    </div>

                    <div class="form-group info-group">
                        {!! Form::label('major', 'Major') !!}
                        {!! Form::input('text', 'major', null, array('placeholder' => 'What is your major? (eg, Painting)', 'class' => 'form-control')) !!}
                    </div>

                    <div class="form-group info-group">
                        {!! Form::label('experience', 'CLC Experience') !!}
                        {!! Form::input('text', 'experience', null, array('placeholder' => 'Brag about it here. (eg, client, quarter, etc.)', 'class' => 'form-control')) !!}
                    </div>
            </div>
        </section>

        <section class="social editable">
            <ul>
                <li>
                    <div class="popup{{ $user->linkedin ? ' filled' : '' }}">
                        {!! Form::label('linkedin', 'Linked In') !!}
                        <div class="url-field">
                            <span contenteditable="true" class="precede">linkedin.com/in/</span><span contenteditable="true" class="url" data-placeholder="username">{{ $user->linkedin }}</span>
                        </div>
                        {!! Form::input('hidden', 'linkedin') !!}
                    </div>
                    <a href="#"><span class="icon-linkedin"></span></a>
                </li>
                <li>
                    <div class="popup{{ $user->instagram ? ' filled' : '' }}">
                        {!! Form::label('instagram', 'Instagram') !!}
                        <div class="url-field">
                            <span contenteditable="true" class="precede">instagram.com/</span><span contenteditable="true" class="url" data-placeholder="username">{{ $user->instagram }}</span>
                        </div>
                        {!! Form::input('hidden', 'instagram') !!}
                    </div>
                    <a href="#"><span class="icon-instagram"></span></a>
                </li>
                <li>
                    <div class="popup{{ $user->twitter ? ' filled' : '' }}">
                        {!! Form::label('twitter', 'Twitter') !!}
                        <div class="url-field">
                            <span contenteditable="true" class="precede">twitter.com/</span><span contenteditable="true" class="url" data-placeholder="username">{{ $user->twitter }}</span>
                        </div>
                        {!! Form::input('hidden', 'twitter') !!}
                    </div>
                    <a href="#"><span class="icon-twitter"></span></a>
                </li>
                <li>
                    <div class="popup{{ $user->facebook ? ' filled' : '' }}">
                        {!! Form::label('facebook', 'Facebook') !!}
                        <div class="url-field">
                            <span contenteditable="true" class="precede">facebook.com/</span><span contenteditable="true" class="url" data-placeholder="username">{{ $user->facebook }}</span>
                        </div>
                        {!! Form::input('hidden', 'facebook') !!}
                    </div>
                    <a href="#"><span class="icon-facebook"></span></a>
                </li>
                <li>
                    <div class="popup{{ $user->public_email ? ' filled' : '' }}">
                        {!! Form::label('public_email', 'Email') !!}
                        <div class="url-field">
                            <span contenteditable="true" class="precede"></span><span contenteditable="true" class="url" data-placeholder="Add your email">{{ $user->public_email }}</span>
                        </div>
                        {!! Form::input('hidden', 'public_email') !!}
                    </div>
                    <a href="#"><span class="icon-email"></span></a>
                </li>
                <li>
                    <div class="popup{{ $user->website ? ' filled' : '' }}">
                        {!! Form::label('website', 'Website') !!}
                        <div class="url-field">
                            <span contenteditable="true" class="precede">http://</span><span contenteditable="true" class="url" data-placeholder="coolwebsite.com">{{ $user->website }}</span>
                        </div>
                        {!! Form::input('hidden', 'website') !!}
                    </div>
                    <a href="#"><span class="icon-web"></span></a>
                </li>
            </ul>
        </section>

        {!! Form::close() !!}
        
    </div>
@endsection

@section('page-js')
    <script type="text/javascript">
    $(function() {

        // get the photo holder
        var holder = $('.avatar.frame');
        var fileInput = holder.find('input[type="file"]');

        // Save button
        $('#user-save').click(function(e) {
            e.preventDefault();

            $('#user-form').submit();
        });

        // Change Avatar Button
        $('#change-avatar').click(function(e) {
            e.preventDefault();

            fileInput.trigger('click');
        });

        // Change Avatar Button
        $('#delete-avatar').click(function(e) {
            e.preventDefault();

            // tell the server to remove any 
            // existing photo file when we submit
            $('#remove-avatar').val(true);

            // Clear the file input by replacing it
            fileInput.replaceWith( fileInput = fileInput.clone( true ) );

            // Remove any background image
            holder.attr('style', 'background-image:url("");');

            // Show the default avatar
            holder.addClass('default').removeClass('dropped');

            // hide the delete button (nothing to delete anymore)
            $(this).hide();
        });

        // Drop states
        holder.on('dragover', function() {
            holder.addClass('hover');
        }).on('dragleave', function() {
            holder.removeClass('hover');
        });

        // Handle Dropping image
        fileInput.on('change', function(e) {
            var file = e.target.files[0];
            
            // States if needed
            holder.removeClass('hover default');
            holder.addClass('dropped');
            // $('#remove-avatar').val('');
            
            // ABORT, not an image
            // Consider a new way of alerting
            if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
              return alert('File type not allowed.');
            }
            
            // load the image
            var reader = new FileReader(file);
            reader.readAsDataURL(file);

            reader.onload = function(e) {
              var data = e.target.result

              // Add it in
              holder.attr('style', 'background-image:url("'+data+'");');

              // Show the delete button
              $('#delete-avatar').show();
            };
        });

        // Social Media Edit
        // custom close event for popup
        $('.popup')
        .on('popup:endedit', function() {
            var popup = $(this);
            var input = popup.find('input');

            // blur the fake field
            popup.find('.url').blur();

            // change the input value to the editable span's content
            popup.find('input').val( popup.find('.url').html() );

            // update the icon
            popup.toggleClass('filled', (input.val() !== ''));
        })
        .on('popup:close', function() {
            // trigger the save just in case
            $(this).trigger('popup:endedit');

            // hide the popup
            $(this).removeClass('active');
        });

        $(document).on('click touchend', function() {
            $('.popup.active').trigger('popup:close');
        });

        $('.social .url-field').click(function() {
            $(this).children('.url').focus();
        }).keypress(function (e) {
            if (e.which == 13) {
                // trigger end edit
                $(this).closest('.popup').trigger('popup:endedit');
                return false;
            }
        });

        $('.social.editable a').click(function(e) {
            e.preventDefault();
            e.stopPropagation();

            $('.popup.active').trigger('popup:close');

            $(this).prev().addClass('active');
        });

        $('.social.editable .popup').on('click touchend',function(e) {
            e.stopPropagation();
        });

    });
    </script>
@endsection