@if ( $user->trashed() )
    <span class="missing-user">{{ $article->user->name }}</span>
@else
    <a href="{{ url('user', $user->slug) }}">{{ $user->name }}</a>
@endif

@if ( isset($commentAnchor) )
    <a href="#{{ $commentAnchor }}" id="{{ $commentAnchor }}" class="comment-anchor">#</a>
@endif