<div class="cd-popup" role="alert">
    <div class="cd-popup-container">
        @section('message')
            <p>Are you sure you want to delete this element?</p>
        @show
        <ul class="cd-buttons">
            @section('buttons')
                <li><a href="#0">Yes</a></li>
                <li><a href="#0">No</a></li>
            @show
        </ul>
        <a href="#0" class="cd-popup-close img-replace">Close</a>
    </div> <!-- cd-popup-container -->
</div> <!-- cd-popup -->