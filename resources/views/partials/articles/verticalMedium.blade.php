<article class="article-preview vertical-medium{{ $article->superBumped ? ' super-bumped' : '' }}{{ $article->draftClass }}">
    <div class="article-thumbnail hover-image">
        <img class="super" src="{{ asset('img/emoji/bump-super.svg') }}">
        <a class="mask" href="{{ url('article', $article->slug) }}">
            <img class="spacer" src="{{ asset('img/aspect-wide.gif') }}">
            <div class="background" style="background-image: url('{{ $article->photo }}');"></div>
            @if ( $article->draftClass )
                <span class="draft-info">Under Review</span>
            @endif
        </a>
    </div>
    <div class="details">
        @if( Auth::user()->can('set-featured-content') )
            <div class="admin-control">
                <a href="#" class="star-control" data-id="{{ $article->id }}"><div class="{{ $article->featureClasses }}"><img src="{{ asset('img/aspect-square.gif') }}"></div></a>
            </div>
        @endif
        <a href="{{ url('article', $article->slug) }}" class="title"><h2>{{ $article->title }}</h2></a>
        <ul class="user-meta">
            <li class="user-name">
                @include('partials.user-link', ['user' => $article->user])
            </li>
            <li class="time">{{ $article->date_diff }}</li>
        </ul>
    </div>
</article>