@extends('partials.userblock')

@section('user-block-header')
    @parent
@overwrite

@section('user-block-content')
    <p class="time">{{ $comment->updated_at->diffForHumans() }}</p>
    <div class="message">{{ $comment->message }}</div>
    @if ( $currentUser->can('moderate-comment') || $currentUser->id == $user->id )
        <a class="delete-comment" data-comment-id="{{ $comment->id }}" href="#">Delete</a>
    @endif
@overwrite