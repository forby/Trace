<article class="article-preview horizontal-medium{{ $article->superBumped ? ' super-bumped' : '' }}">
    <div class="article-thumbnail hover-image">
        <img class="super" src="{{ asset('img/emoji/bump-super.svg') }}">
        <a class="mask" href="{{ url('article', $article->slug) }}">
            <img class="spacer" src="{{ asset('img/aspect-normal.gif') }}">
            <div class="background" style="background-image: url('{{ $article->photo }}');"></div>
            {{-- <span class="hover-info">{{ $article->likeCount }}</span> --}}
        </a>
    </div>
    <div class="details">
        <a href="{{ url('article', $article->slug) }}" class="title"><h2>{{ $article->title }}</h2></a>
        {!! str_limit( $article->overview) !!}
        <ul class="user-meta">
            <li class="user-name"><a href="{{ url('user', $article->user->slug) }}">{{ $article->user->name }}</a></li>
            <li class="time">{{ $article->date_diff }}</li>
            <li class="comments">
                <a href="{{ url('article', $article->slug) }}#comments">
                    {{ $article->comments->count() }} <span class="icon-comment"></span>
                </a>
            </li>
            <li class="bumps{{ $article->liked() ? ' bumped' : '' }}">
                {{ $article->likeCount }} <span class="icon-bump"></span>
            </li>
        </ul>
    </div>
</article>