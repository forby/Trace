@if ( $articles->lastPage() > 1 )
    <div class="pagination">
        <div class="previous">
            @if ( $articles->currentPage() > 1 )
                <a href="{{ $articles->previousPageUrl() }}">&larr; Previous page</a>
            @endif
        </div>
        
        <div class="page">
            {{ $articles->currentPage() }} of {{ $articles->lastPage() }}
        </div>

        <div class="next">
            @if ( $articles->hasMorePages() )
                <a href="{{ $articles->nextPageUrl() }}">Next page &rarr;</a>
            @endif
        </div>
    </div>
@endif