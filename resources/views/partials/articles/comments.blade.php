<section class="comments" id="comments">
    @foreach ($article->comments as $comment)
        @include('partials.articles.comment', [
            'class' => $comment->commentClasses, 
            'currentUser'=> $user, 
            'user' => $comment->user, 
            'commentAnchor' => $comment->id
        ])
    @endforeach
</section>