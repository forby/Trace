<article class="article-preview vertical-feature{{ $article->superBumped ? ' super-bumped' : '' }}">
    <div class="details">
        <a href="{{ url('article', $article->slug) }}" class="title"><h2>{{ $article->title }}</h2></a>
    </div>

    <header>

        {{-- User Block --}}
        @include('partials.userblock', ['user' => $article->user])

    
        <ul class="user-meta">
            <li class="comments">
                <a href="{{ url('article', $article->slug) }}#comments">{{ $article->comments->count() }} <span class="icon-comment"></span></a>
            </li>
            <li class="bumps{{ $article->liked() ? ' bumped' : '' }}">{{ $article->likeCount }} <span class="icon-bump"></span></li>
        </ul>

    </header>

    <div class="article-thumbnail">
        <img class="super" src="{{ asset('img/emoji/bump-super.svg') }}">
        <a class="mask" href="{{ url('article', $article->slug) }}">
            <img class="spacer" src="{{ asset('img/aspect-wider.gif') }}">
            <div class="background" style="background-image: url('{{ $article->photo }}');"></div>
            <span class="hover-info">{{ $article->likeCount }}</span>
        </a>
    </div>
    <div class="overview">
        {!! str_limit( $article->overview, 100) !!}
    </div>
    <a href="{{ url('article', $article->slug)  }}" class="continue">Continue reading &rarr;</a>
</article>

<hr>