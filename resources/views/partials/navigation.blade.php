<nav class="main-nav">
    <ul class="right-nav">
        @if ( $user->can('set-featured-content') )
            <li><a href="{{ url('/feature/edit') }}">Edit Feature</a></li>
        @endif
        @if ( $user->can('moderate-story') )
            <li><a href="{{ url('approve') }}">Approve Stories <span class="count">{{ $approve_count }}</span></a></li>
        @endif
        <li><a href="{{ url('/articles/create') }}">Write</a></li>
        <li class="has-children">
            <a href="#">Categories</a>

            <div class="is-hidden category-nav secondary-nav ul-wrap">
                <ul>
                    <li class="go-back"><a href="#0">Back</a></li>
                    @foreach ($categories as $category)
                        <li><a href="{{ url('/category', $category->slug) }}">{{ $category->name }} <span class="count">{{ $category->count }}</span></a></li>
                    @endforeach
                </ul>
            </div>
        </li>
    </ul>
</nav>