<div class="container">
    <hr class="main-footer-divider">

    <nav class="main-footer-nav">
        <ul>
            {{-- <li><a href="{{ url('help') }}">Help</a></li> --}}
            <li><a href="{{ url('about') }}">About</a></li>
            <li><a href="{{ url('legal') }}">Legal</a></li>
        </ul>
    </nav>
    
    <p class="copyright">&copy; 2015 SCAD</p>
</div>