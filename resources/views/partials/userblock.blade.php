<div class="user-block {{{ $class or '' }}}">
    @if ( $user )
        @include('partials.user-avatar-small')
        <div class="user-meta">
            @section('user-block-header')
                @include('partials.user-link')
            @show
            @section('user-block-content')
                @if( isset( $article ) )
                    <p class="time">{{ $article->created_at->format('F j') }}</p>
                @endif
            @show
        </div>
    @endif
</div>