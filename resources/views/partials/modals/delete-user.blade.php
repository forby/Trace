@extends('partials.modal')

@section('message')
    <p>Are you sure you want to delete {{ $user->name }}?</p>
@endsection

@section('buttons')
    <li><a href="#" id="delete-user">Definitely</a></li>
    <li><a href="#" class="cd-popup-dismiss">Nope</a></li>
@endsection