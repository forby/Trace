@extends('partials.modal')

@section('message')
    <p>Are you sure you want to delete this article?</p>
@endsection

@section('buttons')
    <li><a href="#" id="delete-article">Definitely</a></li>
    <li><a href="#" class="cd-popup-dismiss">Nope</a></li>
@endsection