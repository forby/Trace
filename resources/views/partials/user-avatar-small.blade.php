@if ( $user->trashed() )
    <div class="user-avatar">
        <a>
            <img class="spacer" src="{{ asset('img/aspect-square.gif') }}">
            <div class="background" style="background-image: url('{{ $user->avatar_image }}');"></div>
        </a>
    </div>
@else
    <div class="user-avatar">
        <a href="{{ url('user', $user->slug) }}">
            <img class="spacer" src="{{ asset('img/aspect-square.gif') }}">
            <div class="background{{ $user->avatar ? ' photo' : '' }}" style="background-image: url('{{ $user->avatar_image }}');"></div>
        </a>
    </div>
@endif