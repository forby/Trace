<div class="header-wrap">
    {{-- Logo --}}
    <a class="header-logo" href="{{ url('/') }}"><img src="{{ asset('img/trace.svg') }}"></a>
    
    {{-- Right-side header buttons --}}
    @if ( Auth::check() )
        <ul class="header-buttons">
            <li><a class="search-trigger" href="#"><span></span></a></li>
            <li>
                <a class="profile-trigger" href="#">
                    <div class="user-avatar">
                        <img class="spacer" src="{{ asset('img/aspect-square.gif') }}">
                        <div class="background{{ $user->avatar ? ' photo' : '' }}" style="background-image: url('{{ $user->avatarImage }}');"></div>
                    </div>
                </a>
                <ul>
                    <li class="name">{{ $user->name }}</li>
                    <li><a href="{{ url('user', $user->slug) }}">My Profile</a></li>
                    <li><a href="{{ url('settings') }}">Settings</a></li>
                    @if ( $user->can('manage-users') )
                        <li><a href="{{ url('manage-users') }}">Manage Users</a></li>
                    @endif
                    <li><a href="{{ url('/logout') }}">Sign Out</a></li>
                </ul>
            </li>
            <li><a class="nav-trigger" href="#"><span></span></a></li>
        </ul>
    @endif
</div>