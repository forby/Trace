<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('articles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();

			// Define relationship of User owning an Article
			// delete this when user is deleted
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

			// Define relationship of Category owning an Article
			// delete this when category is deleted
			$table->integer('category_id')->unsigned();
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

			$table->softDeletes();
			$table->string('title');
			$table->text('overview');
			$table->text('example');
			$table->text('advice');
			$table->string('photo');
			$table->boolean('featured')->default(0);
			$table->timestamp('featured_on');
			$table->boolean('published')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('articles');
	}

}
