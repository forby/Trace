<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Filesystem\Filesystem;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			// Basic Details
			$table->increments('id');

			// MySCAD Details
			$table->string('scad_id', 9)->unique();
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email');

			// Trace Details
			$table->string('password', 60);
			$table->string('pro_title');
			$table->string('hometown');
			$table->string('major');
			$table->string('experience');
			// $table->boolean('confirmed')->default(0);
            // $table->string('confirmation_code')->nullable();
            $table->boolean('email_notify')->default(1);

			// Avatar
			$table->string('avatar');
			$table->string('avatar_default');

			//  Social
            $table->string('linkedin');
            $table->string('instagram');
            $table->string('twitter');
            $table->string('facebook');
            $table->string('public_email');
            $table->string('website');
			
			$table->softDeletes();
			$table->rememberToken();
			$table->timestamps();
		});

		// Remove folders if they exist
		if ( File::isDirectory('public/img/articles') ) {
			File::deleteDirectory('public/img/articles');
		}
		if ( File::isDirectory('public/img/users') ) {
			File::deleteDirectory('public/img/users');
		}

		// Prepare images folder
		File::makeDirectory('public/img/articles');
		File::makeDirectory('public/img/users');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');

		File::deleteDirectory('public/img/articles');
		File::deleteDirectory('public/img/users');
	}

}
