<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSluggableColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Articles Slug
		Schema::table('articles', function(Blueprint $table)
		{
			$table->string('slug')->nullable();
		});

		// Users Slug
		Schema::table('users', function(Blueprint $table)
		{
			$table->string('slug')->nullable();
		});

		// Category Slug
		Schema::table('categories', function(Blueprint $table)
		{
			$table->string('slug')->nullable();
		});

		// Tag Slug
		Schema::table('tags', function(Blueprint $table)
		{
			$table->string('slug')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop Articles Slug
		Schema::table('articles', function(Blueprint $table)
		{
			$table->dropColumn('slug');
		});

		// Drop Users Slug
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropColumn('slug');
		});

		// Drop Categories Slug
		Schema::table('categories', function(Blueprint $table)
		{
			$table->dropColumn('slug');
		});

		// Drop Tags Slug
		Schema::table('tags', function(Blueprint $table)
		{
			$table->dropColumn('slug');
		});
	}

}
