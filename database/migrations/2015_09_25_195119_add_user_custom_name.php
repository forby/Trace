<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserCustomName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Add the column
        Schema::table('users', function(Blueprint $table)
        {
            $table->string('custom_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop the column
        Schema::table('users', function(Blueprint $table)
        {
            $table->dropColumn('custom_name');
        });
    }
}
