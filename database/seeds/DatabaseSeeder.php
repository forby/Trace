<?php

use App\Tag;
use App\User;
use App\Role;
use App\Article;
use App\Comment;
use App\Feature;
use App\Category;
use Carbon\Carbon;
use App\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        $this->call('RoleAndPermissionSeeder');

        // Dummy Content
        if (App::environment('local')) {
            $this->call('UserTableSeeder');
            $this->call('TagTableSeeder');
        } else {
            $this->call('UserTableStagingSeeder');
        }

        $this->call('CategoryTableSeeder');

        // Dummy Content
        if (App::environment('local')) {
            $this->call('ArticleTableSeeder');
        }
		
		$this->call('FeatureTableSeeder');
	}

}

class RoleAndPermissionSeeder extends Seeder
{
    public function run()
    {
        // Clean up tables
        DB::table('permission_role')->delete();
        DB::table('permissions')->delete();
        DB::table('role_user')->delete();
        DB::table('roles')->delete();

        // Make Roles
        $admin               = new Role();
        $admin->name         = 'admin';
        $admin->display_name = 'Administrator';
        $admin->description  = 'User is allowed to manage and edit all aspects of the site';
        $admin->save();

        $editor               = new Role();
        $editor->name         = 'editor';
        $editor->display_name = 'Editor';
        $editor->description  = 'User is allowed to manage posted stories and moderate comments';
        $editor->save();

        $user               = new Role();
        $user->name         = 'user';
        $user->display_name = 'User';
        $user->description  = 'User is allowed to submit stories and post comments';
        $user->save();

        // Make Permissions
        $createStory = Permission::create([
            'name'         => 'create-story',
            'display_name' => 'Create Story',
            'description'  => 'Create a new story'
        ]);

        $moderateStory = Permission::create([
            'name'         => 'moderate-story',
            'display_name' => 'Moderate Story',
            'description'  => 'Moderate and approve any stories'
        ]);

        $postAsUsers = Permission::create([
            'name'         => 'post-as-users',
            'display_name' => 'Post as Users',
            'description'  => 'Write a new post as a different user while in development'
        ]);

        $setFeaturedContent = Permission::create([
            'name'         => 'set-featured-content',
            'display_name' => 'Set Featured Content',
            'description'  => 'Handpick featured content for display in featured sections'
        ]);

        $postComment = Permission::create([
            'name'         => 'post-comment',
            'display_name' => 'Post Comment',
            'description'  => 'Post a new comment'
        ]);

        $moderateComment = Permission::create([
            'name'         => 'moderate-comment',
            'display_name' => 'Moderate Comment',
            'description'  => 'Moderate any comment'
        ]);

        $manageUsers = Permission::create([
            'name'         => 'manage-users',
            'display_name' => 'Manage Users',
            'description'  => 'Allow modification of any user'
        ]);

        $removeArticles = Permission::create([
            'name'         => 'remove-articles',
            'display_name' => 'Remove Articles',
            'description'  => 'Remove any Articles'
        ]);

        // Assign Permissions
        $admin->attachPermission( $setFeaturedContent );
        $admin->attachPermission( $manageUsers );

        $editor->attachPermission( $moderateStory );
        $editor->attachPermission( $moderateComment );
        $editor->attachPermission( $postAsUsers );
        $editor->attachPermission( $removeArticles );

        $user->attachPermission( $createStory );
        $user->attachPermission( $postComment );
    }
}

class UserTableSeeder extends Seeder
{
	/**
	 * Seed the User Table
	 *
	 * @return void
	 */
	public function run() 
	{
		// Remove all users from the DB
		DB::table('users')->delete();

        // Get faker
        $faker = Faker\Factory::create();

        $this->command->info('Making Users');

        // Me
        /*$avatar = $faker->image('public/img/users', 200, 200);
        $me = User::create(
        	array(
                'first_name' => 'Jonathan',
                'last_name' => 'Forby',
                'scad_id' => '000650331',
        		'email' => 'me@jonathanforby.com',
                'pro_title' => 'Professional Child',
                'hometown' => 'Savannah, GA',
                'avatar' => str_replace('public', '', $avatar)
    		)
	);*/

        // Make Admin
        // $me->attachRole(1);

        /**
         * Make a dummy Admin with no profile image
         */
        $dummy_admin = User::create(
            array(
                'first_name' => 'Josh',
                'last_name' => 'Smosh',
                'email' => 'admin@test.com',
                'scad_id' => '000' . mt_rand(100000, 999999)
            )
        );
        $dummy_admin->attachRole(3);
        $dummy_admin->attachRole(2);
        $dummy_admin->attachRole(1);


        /**
         * Make a dummy Editor with no profile image
         */
        $dummy_editor = User::create(
            array(
                'first_name' => 'Amanda',
                'last_name' => 'Banana',
                'email' => 'editor@test.com',
                'scad_id' => '000' . mt_rand(100000, 999999)
            )
        );
        $dummy_editor->attachRole(3);
        $dummy_editor->attachRole(2);

        /**
         * Make a dummy Editor with no profile image
         */
        $dummy_user = User::create(
            array(
                'first_name' => 'Frank',
                'last_name' => 'Tank',
                'email' => 'user@test.com',
                'scad_id' => '000' . mt_rand(100000, 999999)
            )
        )->attachRole(3);

        // Other
        $avatar = $faker->image('public/img/users', 200, 200);
        $editor = User::create(
            array(
                'first_name' => 'John',
                'last_name' => 'Smith',
                'email' => 'test@test.com',
                'scad_id' => '000' . mt_rand(100000, 999999),
                'avatar' => str_replace('public', '', $avatar)
            )
        );

        // Editor Role
        $editor->attachRole(3);
        $editor->attachRole(2);

    	// create other random users
    	foreach( range( 1, 10 ) as $index )
    	{
            // $avatar = $faker->image('public/img/users', 200, 200);
    		$user = User::create([
                'first_name' => $faker->firstName,
			'last_name' => $faker->lastName,
    			'email' => $faker->email,
                'scad_id' => '000' . mt_rand(100000, 999999)
                // 'avatar' => str_replace('public', '', $avatar)
    		]);

            // User role
            $user->attachRole(3);
    	}
	}
}

class UserTableStagingSeeder extends Seeder
{
    /**
     * Seed the User Table
     *
     * @return void
     */
    public function run() 
    {
        // Remove all users from the DB
        DB::table('users')->delete();

        // Add the Pre-defined authors
        $this->command->info('Making Initial Author Accounts');

        // Get the Authors
        $authors = Config::get('users')['authors'];

        // Create the accounts
        foreach ($authors as $author) {

            // Prepare names
            $words = explode( ' ', $author['name'] );
            $last_name = array_pop( $words );
            $first_name = implode( ' ', $words );

            // Create User
            User::create(
                array(
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'scad_id' => strval( $author['scad_id'] )
                )
            )->attachRole(3);
        }
    }
}

class TagTableSeeder extends Seeder
{
	public function run()
	{
		// Remove all tags from the DB
		DB::table('tags')->delete();

		// Get Faker
		$faker = Faker\Factory::create();

        $this->command->info('Making Initial 10 Tags');

		// Make 10 tags
        Tag::create(['name' => 'Planning']);
        Tag::create(['name' => 'Management']);
        Tag::create(['name' => 'Design']);
        Tag::create(['name' => 'Art']);
        Tag::create(['name' => 'Art Supplies']);
        Tag::create(['name' => 'Random']);
        Tag::create(['name' => 'Job Searching']);
        Tag::create(['name' => 'Get it Done']);
        Tag::create(['name' => 'Whatever']);
		Tag::create(['name' => 'Color Theory']);
	}
}

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        // Remove all categories from the DB
        DB::table('categories')->delete();

        $this->command->info('Making Initial Categories');

        // Create 5 basic categories
        Category::create(['name' => 'Communication']);
        Category::create(['name' => 'Research']);
        Category::create(['name' => 'Deliverables']);
        Category::create(['name' => 'Management']);
        Category::create(['name' => 'Presentation']);
        Category::create(['name' => 'Resources']);
    }
}

class ArticleTableSeeder extends Seeder {
    public function run()
    {
        // Remove all articles from the DB
        DB::table('articles')->delete();

        // Get Faker
        $faker = Faker\Factory::create();

        $this->command->info('Making some articles');

        // Make a bunch of articles
        foreach( range( 1, 40 ) as $index )
        {
            $photo = $faker->image('public/img/articles', 1000, 400);

            $this->command->info('Creating article #' . $index);

            $article = Article::create([
                'created_at'    => $faker->dateTimeBetween('-1 year'),
                'title'         => $faker->sentence(4),
                'overview'      => $faker->paragraph,
                'example'       => $faker->paragraph,
                'advice'        => $faker->paragraph,
                'user_id'       => rand( 1, 9 ),
                'category_id'   => rand( 1, 5 ),
                'photo'         => str_replace('public', '', $photo),
                'published'     => rand(0, 1)
            ]);

            // $this->command->info('Attaching a random tag to this article');

            // Attaching a randomtag
            $article->tags()->attach( rand(1,10) );

            // make a few random comments
            // if the article has been published
            if ( $article->published === 1 ) {
                $rand = rand( 1,3 );
                foreach( range( 1, $rand ) as $i )
                {
                    $author = App\User::find( rand(1, 10) );
                    
                    $comment = new App\Comment;
                    $comment->message = $faker->paragraph(2);
                    $comment->user_id = $author->id;

                    $article->comments()->save( $comment );
                }

                // add some random (unattributed) likes
                $rand = rand( 1,3 );
                foreach( range( 1, $rand ) as $i )
                {
                    $article->like( rand(1, 15) );
                }
            }
        }
    }
}

class FeatureTableSeeder extends Seeder {
	public function run()
	{
		// Remove all articles from the DB
		DB::table('features')->delete();

        // Make the default Feature
        Feature::create([
            'title'         => 'Welcome to Trace',
            'description'   => 'Trace is an exclusive ecosystem provided by SCAD’s Collaborative Learning Center for professional best practices and design processes.',
            'button_text'   => 'Read More',
            'photo'         => 'img/temp_feature.jpg',
            'url'           => 'about',
            'active'        => '1',
            'default'       => '1'
        ]);
		
	}
}

